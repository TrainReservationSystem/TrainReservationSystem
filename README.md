# Train Reservation System

Train reservation system made for the sake of database final project 4th semester.
All of the data used is randomly generated.

## Database Group Assignment - Train Reservation System Group 2017/2018 KI
### Group Member
 - Vincentius Aditya Sundjaja – 1606863472
 - Rizki Maulana Rahmadi – 1606899970
 - A. Bayusuto Wanengkirtyo – 1606899863

### Division of Tasks
 - Vincentius Aditya Sundjaja
	 - Implements Feature 1: Login & Logout
	 - Implements Feature 6: View train schedule
     - Implements Feature 7: Booking train schedule
     - Implements Feature 8: View e-ticket
 - Rizki Maulana Rahmadi
	 - Implements Feature 5: Input attendance
	 - Implements Feature 9: Order food
 - A. Bayusuto Wanengkirtyo
	 - Implements Feature 2: User registration
     - Implements Feature 3: View Profile
     - Implements Feature 4: Update station

### Deployment
This application is deployed using heroku server. Here is the url: https://train-reservation-system.herokuapp.com
