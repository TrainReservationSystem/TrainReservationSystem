--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:11:40

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 219 (class 1259 OID 50370)
-- Name: user_; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE user_ (
    username character varying(20) NOT NULL,
    email character varying(20) NOT NULL,
    password character varying(20) NOT NULL,
    phone_no character varying(20) NOT NULL,
    address_no character varying(20) NOT NULL,
    street character varying(20) NOT NULL,
    village character varying(20) NOT NULL,
    sub_district character varying(20) NOT NULL,
    zipcode character varying(20) NOT NULL
);


ALTER TABLE user_ OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 50370)
-- Dependencies: 219
-- Data for Name: user_; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO user_ VALUES ('Ainhoa', 'Atkins@db.com', 'test', '749-0744', '5749WZ', '91 N Broadway', 'Alberta', 'New Brunswick', '14326');
INSERT INTO user_ VALUES ('Ted6', 'Cotton@db.com', 'test', '1-895-163-2459', '1135ZV', '650 Business Center', 'New Mexico', 'Idaho', '61702');
INSERT INTO user_ VALUES ('Theodore5', 'Johnston@db.com', '8JwzS', '1-875-4031', '8135ML', '4328 Lindstrom Rd', 'Wyoming', 'Alabama', '07808');
INSERT INTO user_ VALUES ('Anna', 'Wagner@db.com', '05ZgECak1hXF', '529-085-2962', '9105DP', '74 Sunnyside Ave.', 'Kansas', 'California', '72162');
INSERT INTO user_ VALUES ('Ulla3', 'Wiley@db.com', 'H6nKsmgVcmjGP6V', '839-2124', '8388WK', '8345 Muzzatta Way', 'Vermont', 'Alaska', '25896');
INSERT INTO user_ VALUES ('Alexander795', 'Mathis@db.com', 'wzwdtDgrl8avYGl2NP', '1-735-1408', '3103AF', '69 E. Plumb Lane', 'Virginia', 'Hawaii', '73687');
INSERT INTO user_ VALUES ('Lucas30', 'Moody@db.com', 'et2i', '709-8127', '3450WE', '469 Southfern Ct', 'Oklahoma', 'Nova Scotia', '95490');
INSERT INTO user_ VALUES ('Ross5', 'Bates@db.com', '76Q1605zjpp0dFj3', '1-425-9317', '6780TB', '985 Bryant Street', 'Wyoming', 'Quebec', '98788');
INSERT INTO user_ VALUES ('Paula9', 'Everett@db.com', 'gjNb', '1-489-733-9892', '7535WN', '9 Springfield Ave', 'Oregon', 'Kansas', '71660');
INSERT INTO user_ VALUES ('Hugo', 'Ellison@db.com', 'W', '1-438-897-2987', '8319SM', '151 Woodside Drive', 'Maine', 'New Brunswick', '10228');
INSERT INTO user_ VALUES ('Matthew93', 'Raymond@db.com', 'Mj3z6JcTQkJxx3WA8Z', '1-958-997-5462', '1262AU', '8000 Main St.', 'New Brunswick', 'British Columbia', '91850');
INSERT INTO user_ VALUES ('Bram', 'Goff@db.com', '8RlcDVOiglNK', '824-7773', '9012VL', '1 Crookston Lane', 'Maine', 'Oregon', '62916');
INSERT INTO user_ VALUES ('Louise4', 'Talley@db.com', 'Fs0QJv1JiaAH', '073-675-2429', '0945XP', '1469 Craftland Road', 'Arkansas', 'British Columbia', '50951');
INSERT INTO user_ VALUES ('Nigel2', 'Howell@db.com', 'uU5Zl', '1-676-553-1026', '7405KO', '6672 Ninth Avenue', 'Maryland', 'Nova Scotia', '15258');
INSERT INTO user_ VALUES ('Al', 'Vang@db.com', 'chb', '413-734-0933', '3571EX', '137 Sungrave Lane', 'New York', 'New Jersey', '56961');
INSERT INTO user_ VALUES ('Kaylee5', 'Benjamin@db.com', 'oDrPfqaFUoGtHQY', '598-901-4822', '3277PJ', '4737 Tremont Street', 'Indiana', 'Vermont', '69714');
INSERT INTO user_ VALUES ('Karin', 'Richards@db.com', 'ySvPBLYo0iMgydZ', '1-995-6983', '8130VH', '0955 Jefferson Road', 'Connecticut', 'Newfoundland', '16157');
INSERT INTO user_ VALUES ('Nico362', 'Stone@db.com', 'XL', '1-220-5834', '2774QM', '891 Paddington Lane', 'Quebec', 'Idaho', '08363');
INSERT INTO user_ VALUES ('Lukas45', 'Yang@db.com', 'lM7', '1-887-266-3050', '2830JK', '17 Jefferson Ave', 'Oregon', 'Vermont', '38546');
INSERT INTO user_ VALUES ('Robert61', 'Mendoza@db.com', 'Y61MBWyCvbo', '1-774-741-9822', '7646BT', '26 S. Brackbank Dr', 'Nova Scotia', 'New Jersey', '02808');
INSERT INTO user_ VALUES ('Linnea3', 'Cole@db.com', '088Kc', '1-707-5533', '4310QC', '1 Rockville Pike', 'Kansas', 'Indiana', '63192');
INSERT INTO user_ VALUES ('Giel', 'Hughes@db.com', '4Vu2FImVjs', '1-230-614-3146', '3232OV', '14 Tuckahoe St', 'Tennessee', 'Maine', '60262');
INSERT INTO user_ VALUES ('Ike990', 'Bright@db.com', 'CL2ZP54sJr', '317-913-0144', '1539LN', '203 Northcreek', 'Michigan', 'Prince Edward Island', '76139');
INSERT INTO user_ VALUES ('Sem8', 'Francis@db.com', 'tVLxkcBuG', '469-000-3508', '2830MU', '92 Acorn Dr.', 'Texas', 'Virginia', '04040');
INSERT INTO user_ VALUES ('Maja9', 'Dorsey@db.com', 'pMgpbvKHiQ', '344-9497', '6078OT', '3668 Freeman St', 'Quebec', 'Kentucky', '63014');
INSERT INTO user_ VALUES ('Tommy514', 'Nixon@db.com', 'iRAJIuowaBuU', '1-699-991-7982', '3098HG', '32 Hunt Valley Road', 'North Dakota', 'Minnesota', '35989');
INSERT INTO user_ VALUES ('Niklas109', 'Carson@db.com', 'OQWgD', '1-636-2960', '0912BG', '4 Torin Road', 'Pennsylvania', 'Delaware', '89581');
INSERT INTO user_ VALUES ('Cath', 'Mcneil@db.com', 'GKS', '1-745-1761', '6401PH', '2 Sutter Street', 'Missouri', 'British Columbia', '48687');
INSERT INTO user_ VALUES ('Oliver609', 'Santiago@db.com', 'OYbYlSU', '222-102-5913', '3853PT', '3 W. Sunnyside Ave.', 'Hawaii', 'Wyoming', '41022');
INSERT INTO user_ VALUES ('Guus3', 'Eaton@db.com', 'k7hqyWCrzEEmjAFG', '378-672-8376', '9901KK', '4271 Miranda Drive', 'Yukon', 'Ohio', '75466');
INSERT INTO user_ VALUES ('Cees72', 'Joyner@db.com', 'FvzdxlD8', '666-328-1958', '3489NW', '6 North 86th Street', 'Pennsylvania', 'Missouri', '95750');
INSERT INTO user_ VALUES ('Charlotte619', 'Farrell@db.com', 'yt', '109-792-9289', '8700LF', '483 Elm Drive', 'South Carolina', 'New Mexico', '62099');
INSERT INTO user_ VALUES ('Matthew977', 'Hewitt@db.com', 'gLFd36LuTkUOnznI', '262-883-5950', '6395JG', '4352 Virginia Dr', 'Utah', 'Minnesota', '73181');
INSERT INTO user_ VALUES ('Scottie', 'Mckinney@db.com', 'AeLgPcrB4iESnP62v7', '959-1087', '3945EA', '8 Eisenhower Avenue', 'Illinois', 'Idaho', '80989');
INSERT INTO user_ VALUES ('Martin', 'Kane@db.com', 'd3XkoNR0vvsQ', '942-8644', '9399SW', '492 Ferro Dr', 'Georgia', 'Nova Scotia', '99593');
INSERT INTO user_ VALUES ('Alexander810', 'Anderson@db.com', 'KbRq8t4U', '491-3568', '4316UB', '7 Alder Road', 'Kentucky', 'New York', '22501');
INSERT INTO user_ VALUES ('GertJan342', 'Nichols@db.com', 'b', '1-549-057-7302', '7339NX', '2 Business Center', 'West Territories', 'Alaska', '52232');
INSERT INTO user_ VALUES ('Herb47', 'Woodard@db.com', 'a', '904-0422', '6569TN', '7 Wicksicam Ave', 'Prince Harry Island', 'Kansas', '76408');
INSERT INTO user_ VALUES ('Thomas', 'Sparks@db.com', 'z8l1qO15GPjyKFASXWG', '1-053-658-0730', '6794SI', '8 Mercury Drive', 'Vermont', 'Quebec', '20172');
INSERT INTO user_ VALUES ('Piet', 'Dillard@db.com', 'M', '795-3057', '6096ZU', '858 Perimeter Dr.', 'Idaho', 'Ohio', '22309');
INSERT INTO user_ VALUES ('Eleanor', 'Nunez@db.com', 'uJRT4PrTWJYCMN', '331-183-2606', '4984KN', '1 Fox Squirrel Lane', 'Oklahoma', 'Maine', '55228');
INSERT INTO user_ VALUES ('Dave976', 'Maynard@db.com', 'qB8KEV3aTQyMMJ1B', '1-912-912-2503', '1037KJ', '40 Carnaby Creek', 'Florida', 'Virginia', '11431');
INSERT INTO user_ VALUES ('Thomas5', 'Fields@db.com', 'rmH', '1-709-1442', '6768GF', '794 Serang Place', 'Massachusetts', 'Wyoming', '35423');
INSERT INTO user_ VALUES ('Tyler200', 'Deleon@db.com', 'FTKJ3', '862-8212', '1034ME', '72 Bryant Street', 'Indiana', 'Montana', '46445');
INSERT INTO user_ VALUES ('Cloe694', 'Alvarado@db.com', 'iP', '470-035-7822', '6260LX', '8 Main St.', 'Washington', 'Yukon', '47260');
INSERT INTO user_ VALUES ('Edwyn01', 'Edwards@db.com', 'dbVo5Wph0XiW', '1-645-9719', '4107XP', '2 N. Glenoaks Blvd', 'Ontario', 'Utah', '31457');
INSERT INTO user_ VALUES ('Carlos', 'Hopkins@db.com', 'hLhO6bwdQu5XNbyhIGp', '1-925-6930', '1524KL', '1 Jefferson Ave', 'Massachusetts', 'Nevada', '40515');
INSERT INTO user_ VALUES ('Sigrid308', 'Bird@db.com', 'NWRTzGZPYMzd6oXie', '1-829-2422', '1321FF', '71 North MacArthur', 'Maine', 'Quebec', '92457');
INSERT INTO user_ VALUES ('Lara', 'Fuentes@db.com', 'oHrJsb3OmZLGH5G', '490-5324', '7519AG', '1580 Southwest Blvd', 'New Brunswick', 'Nova Scotia', '81075');
INSERT INTO user_ VALUES ('Edward715', 'Hampton@db.com', 'hGV5GN6ioN1w8jaS', '692-831-4598', '9099HQ', '21 West 28th Street', 'California', 'Utah', '57168');
INSERT INTO user_ VALUES ('Luis', 'Schneider@db.com', 'fbPfzPHr6LFoHaO', '1-930-684-6799', '5660VO', '731 Gascony Pl.', 'New Jersey', 'Missouri', '44078');
INSERT INTO user_ VALUES ('Lauren8', 'Horne@db.com', 'FX54n4QZyMiTvsxmL', '037-2629', '2264WI', '569 Waterford Blvd', 'Missouri', 'Wisconsin', '84535');
INSERT INTO user_ VALUES ('Hannah', 'Landry@db.com', 'hz3', '293-493-4106', '4922RP', '34 Terminal Way', 'New Mexico', 'West Virginia', '63397');
INSERT INTO user_ VALUES ('Lucy89', 'Boyd@db.com', 'c8YoymWP', '730-508-3808', '2549NF', '9 King Salmon Place', 'Montana', 'Yukon', '57038');
INSERT INTO user_ VALUES ('Jordy4', 'Justice@db.com', 'j', '1-118-060-7058', '0064OQ', '8 Floribunda Ave', 'British Columbia', 'Georgia', '75748');
INSERT INTO user_ VALUES ('Jolanda3', 'Singleton@db.com', 'j0snYJ1aMaarQfsL', '208-3615', '4752RP', '4 King Salmon St', 'Nebraska', 'New Hampshire', '72929');
INSERT INTO user_ VALUES ('Ellie', 'Holder@db.com', 'C7L3DDcaXx2GZfxvkS', '1-049-9109', '8312NB', '33 North MacArthur', 'Louisiana', 'Mississippi', '62847');
INSERT INTO user_ VALUES ('Vincent33', 'Key@db.com', '5tqt', '411-2366', '1800FQ', '40 W. Rand Rd.', 'Missouri', 'Oregon', '61402');
INSERT INTO user_ VALUES ('Gillian8', 'Shepherd@db.com', 'KMFjfhytpH2ZNOB2', '1-030-4279', '2961UZ', '92 Maxwell Ave.', 'Colorado', 'Florida', '89202');
INSERT INTO user_ VALUES ('HeroZero', 'Hoover@db.com', 'zY3Aw0YIL12', '308-8223', '0462DX', '8 Fox Squirrel Lane', 'Iowa', 'New York', '32430');
INSERT INTO user_ VALUES ('Jeanette89', 'Bullock@db.com', 'VtyLSjTub', '1-840-090-1168', '8526QV', '2 Carolyn Close', 'Alaska', 'Utah', '77403');
INSERT INTO user_ VALUES ('Lukas353', 'Harrington@db.com', 'O4Nw5zvKFC', '1-596-5900', '6865EM', '617 Station Street', 'Delaware', 'Delaware', '85430');
INSERT INTO user_ VALUES ('Scott9', 'Woods@db.com', 'hXINtdIUpdwMYoykRH', '779-320-2587', '9597SF', '4 Farquar Ave', 'Pennsylvania', 'Nevada', '51542');
INSERT INTO user_ VALUES ('Joop919', 'Sears@db.com', 'gOIP8', '1-847-5740', '4652OK', '27 Quorum Dr', 'Texas', 'Nova Scotia', '21897');
INSERT INTO user_ VALUES ('Margarita', 'Holland@db.com', 'Ml2lkoD86nMeTflEZ', '505-3975', '5096CZ', '7 Sungrave Lane', 'North Carolina', 'Georgia', '62750');
INSERT INTO user_ VALUES ('Scott', 'Padilla@db.com', 'msdlbJqqOTuc', '1-836-1137', '7649VZ', '0 J. Carcione Way', 'District of Columbia', 'South Carolina', '57978');
INSERT INTO user_ VALUES ('Mart7', 'Noel@db.com', '6PaHSSxF', '704-562-0405', '2220DL', '6 Brimming Street', 'Rhode Island', 'North Carolina', '41547');
INSERT INTO user_ VALUES ('Alfons563', 'Humphrey@db.com', 'FdVnq', '271-543-9076', '3873ZR', '329 Woodside Drive', 'Arkansas', 'Wisconsin', '26357');
INSERT INTO user_ VALUES ('Will62', 'Pace@db.com', 'AAej13e0T8frVKbjJ6Z', '561-623-6453', '3623QR', '625 W. Broadway', 'Nova Scotia', 'Virginia', '68936');
INSERT INTO user_ VALUES ('Mary', 'Dickson@db.com', '8M5UAkFdk3QW2Re', '059-437-0072', '1477GY', '6 Brimming Street', 'Illinois', 'Oklahoma', '28876');
INSERT INTO user_ VALUES ('Jessica', 'Vincent@db.com', '2JvDfvt', '1-662-6674', '9793RH', '587 Unbridle Way', 'Tennessee', 'New York', '45033');
INSERT INTO user_ VALUES ('Lincoln', 'Gay@db.com', 'EyNHRTvznys3', '350-3669', '4892VO', '2 Penny Lane', 'Wyoming', 'Oregon', '69153');
INSERT INTO user_ VALUES ('Callum3', 'Powell@db.com', 'Qk', '018-1028', '7806XG', '263 NW 111th Loop', 'Delaware', 'Michigan', '01195');
INSERT INTO user_ VALUES ('Klara25', 'Gibbs@db.com', '82oqD6Kx', '225-5669', '0205OW', '205 Eastman Drive', 'Vermont', 'Wyoming', '13733');
INSERT INTO user_ VALUES ('Claudia', 'Underwood@db.com', 'O7qgid', '014-0475', '4893JU', '867 Weber Street', 'Arkansas', 'Nova Scotia', '92085');
INSERT INTO user_ VALUES ('Nathan', 'Ellis@db.com', 'p', '792-6410', '2525DM', '485 Arlington Road', 'Alberta', 'Montana', '53550');
INSERT INTO user_ VALUES ('Bertje', 'Hays@db.com', 'rliGyYD2SnNDU3bKt6N', '1-800-9629', '0146HD', '4 Chehalis Road', 'Nebraska', 'Wyoming', '58928');
INSERT INTO user_ VALUES ('Dylan', 'Wheeler@db.com', '66f2plEitdl0AbK', '331-380-6633', '5685NE', '02 Greenheath Ave', 'Kentucky', 'Delaware', '04513');
INSERT INTO user_ VALUES ('Richard30', 'Chaney@db.com', 'o', '1-716-9239', '0438PA', '9 Carroll Center Rd', 'Yukon', 'New Jersey', '51439');
INSERT INTO user_ VALUES ('Cecilie393', 'Bradshaw@db.com', 'ETwHIMlpRsLzyBEcTLQ', '168-983-9182', '9828TH', '630 Stewart Ave', 'Maryland', 'Michigan', '83862');
INSERT INTO user_ VALUES ('Liza667', 'Randolph@db.com', '0KGbICxu46', '036-823-1947', '6561HY', '3 Vanderheck', 'Minnesota', 'Maryland', '99075');
INSERT INTO user_ VALUES ('Anton49', 'Phillips@db.com', 'jPRP', '1-118-100-5972', '2468BE', '637 Canyon Crest', 'Mississippi', 'Pennsylvania', '70162');
INSERT INTO user_ VALUES ('Philip', 'Rush@db.com', 'tq', '880-9679', '4413TB', '5327 Craftland Road', 'Michigan', 'Kansas', '92980');
INSERT INTO user_ VALUES ('Camille197', 'Orr@db.com', 'ZYPKiczJYMCjJ', '1-545-5315', '8248WV', '1526 Weber Street', 'Tennessee', 'Wisconsin', '67497');
INSERT INTO user_ VALUES ('Daniel', 'Ruiz@db.com', 'SdPMfzXkcBDmJ', '395-377-2104', '0404JF', '8 Virginia Dr', 'Georgia', 'Georgia', '64036');
INSERT INTO user_ VALUES ('Oscar', 'Morton@db.com', '62D0C', '141-1770', '6048HM', '70 Everglades Way', 'Indiana', 'Maryland', '69566');
INSERT INTO user_ VALUES ('Peggy428', 'Ratliff@db.com', 'tmqQnZ', '1-494-360-5538', '3195GX', '09 South Wacker Dr.', 'Nebraska', 'Pennsylvania', '79001');
INSERT INTO user_ VALUES ('Lu460', 'Forbes@db.com', 'dqbvQMAuZJBS3qM7yk', '1-073-4221', '9166CU', '6 Ninth Avenue', 'Arizona', 'Kansas', '68466');
INSERT INTO user_ VALUES ('Jim', 'Leon@db.com', 'XJ7A4OKo', '358-981-7142', '9727CA', '880 Everglades Road', 'Vermont', 'New Hampshire', '02303');
INSERT INTO user_ VALUES ('Jaclyn', 'Sherman@db.com', '4hPnmE8qtCd', '1-738-0948', '6552GS', '190 Floribunda Ave', 'Georgia', 'Mississippi', '27793');
INSERT INTO user_ VALUES ('Alexander5', 'Solomon@db.com', 'eToF', '286-349-5776', '5610RE', '4 Brooke Valley', 'Vermont', 'Minnesota', '42115');
INSERT INTO user_ VALUES ('Catherine7', 'Gentry@db.com', 'iSEt3BYCOz', '1-022-568-0117', '4492PT', '2 Eisenhower Avenue', 'New York', 'Oregon', '66389');
INSERT INTO user_ VALUES ('Alva094', 'Joseph@db.com', 'D8P8TgQty6JQRdKFt4', '1-539-788-4183', '1735IU', '177 Farquar Ave', 'Michigan', 'Missouri', '83427');
INSERT INTO user_ VALUES ('Iris140', 'Wade@db.com', '4', '1-205-802-6408', '5254IG', '9 Palos Verdes Mall', 'Tennessee', 'Maine', '79368');
INSERT INTO user_ VALUES ('Joanne514', 'Sellers@db.com', 'y63F', '1-508-1857', '1725DF', '73 Linden Ridge Dr.', 'Louisiana', 'New York', '89413');
INSERT INTO user_ VALUES ('Ed744', 'Foley@db.com', 'eVbG0WKcF', '1-130-5824', '8253DB', '506 Corporate Drive', 'Hawaii', 'Pennsylvania', '41940');
INSERT INTO user_ VALUES ('Bertje128', 'Weiss@db.com', 'n5NMZ', '1-043-6439', '5974XI', '35 Nortman Blvd', 'New Brunswick', 'Arkansas', '42256');
INSERT INTO user_ VALUES ('Lu2', 'Ellison2@db.com', '71IZHo0X88VOlhgwP', '1-252-018-3946', '2696ZJ', '2 Brockbank Drive', 'Nebraska', 'Washington', '08503');
INSERT INTO user_ VALUES ('Cian', 'Richards2@db.com', 'vpXLa10Ce4JwUN', '112-2101', '6524KE', '407 Highland Drive', 'Arizona', 'North Carolina', '94462');
INSERT INTO user_ VALUES ('Siska32', 'Mathews@db.com', 'bZoFJDYD4mXkeoa8Iaw', '420-050-1251', '4765TR', '4 Eisenhower Avenue', 'Hawaii', 'Washington', '15837');
INSERT INTO user_ VALUES ('Vincent', 'v@yahoo.co.id', 'test1234', '0987654321', '1', 'A', 'B', 'C', '1234');


--
-- TOC entry 2793 (class 2606 OID 50376)
-- Name: user_ user__email_key; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY user_
    ADD CONSTRAINT user__email_key UNIQUE (email);


--
-- TOC entry 2795 (class 2606 OID 50374)
-- Name: user_ user__pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY user_
    ADD CONSTRAINT user__pkey PRIMARY KEY (username);


-- Completed on 2018-06-02 16:11:40

--
-- PostgreSQL database dump complete
--

