--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:13:14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 236 (class 1259 OID 50660)
-- Name: train_booking; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE train_booking (
    booking_id integer DEFAULT nextval('train_booking_id_seq'::regclass) NOT NULL,
    booking_date date NOT NULL,
    schedule_id integer NOT NULL,
    passenger_id character varying(20) NOT NULL,
    booked_by integer NOT NULL
);


ALTER TABLE train_booking OWNER TO postgres;

--
-- TOC entry 2919 (class 0 OID 50660)
-- Dependencies: 236
-- Data for Name: train_booking; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO train_booking VALUES (1, '2014-02-06', 1, 'n72ttlxPPPSgL3Og', 1);
INSERT INTO train_booking VALUES (2, '2006-04-04', 2, 'cMgT', 2);
INSERT INTO train_booking VALUES (3, '2010-01-14', 3, 'V', 3);
INSERT INTO train_booking VALUES (4, '2003-04-18', 4, 'vZireJe', 4);
INSERT INTO train_booking VALUES (5, '2002-12-31', 5, 'SUg0EK1aeBiVugb6lJ', 5);
INSERT INTO train_booking VALUES (7, '2009-11-21', 7, 'CS7my7LPwTLsWQw8MD', 7);
INSERT INTO train_booking VALUES (8, '2004-12-16', 8, 'tgqV5upFeW', 8);
INSERT INTO train_booking VALUES (9, '2015-05-13', 9, 'bvER4uYgcufoJs', 9);
INSERT INTO train_booking VALUES (10, '2014-04-11', 10, 'lJ2jbqobaJc2K6cFLQb', 10);
INSERT INTO train_booking VALUES (11, '2013-03-05', 11, 'a2BUw4v', 11);
INSERT INTO train_booking VALUES (12, '2012-06-20', 12, 'sYx2uUbr20VA8es', 12);
INSERT INTO train_booking VALUES (13, '2007-01-10', 13, 'vqGuOPx66GLfelxyRy', 13);
INSERT INTO train_booking VALUES (14, '2000-09-07', 14, 'xugfEkR0nx8LEy3M', 14);
INSERT INTO train_booking VALUES (15, '2014-06-28', 15, 'CzGPLAfwo2', 15);
INSERT INTO train_booking VALUES (16, '2013-01-12', 16, 'fOr7HKg2ZtPNMPo5', 16);
INSERT INTO train_booking VALUES (18, '2003-03-20', 18, 'jqmhVpNU', 18);
INSERT INTO train_booking VALUES (19, '2017-01-15', 19, 'P20ji6tIZewph2', 19);
INSERT INTO train_booking VALUES (20, '2010-10-14', 20, 'Fj7BOmB', 20);
INSERT INTO train_booking VALUES (21, '2017-03-20', 1, 'uDk73', 21);
INSERT INTO train_booking VALUES (22, '2014-09-01', 2, 'GslmvxyddV74', 22);
INSERT INTO train_booking VALUES (23, '2014-09-22', 3, 'aZGbcK4nUk1y', 23);
INSERT INTO train_booking VALUES (24, '2003-03-31', 4, 'QmgCPt3djqFuFCIF', 24);
INSERT INTO train_booking VALUES (25, '2017-01-18', 5, 'kSTHzol2', 25);
INSERT INTO train_booking VALUES (26, '2016-06-15', 6, 'AfM2ujAouvyVFR', 26);
INSERT INTO train_booking VALUES (27, '2001-06-03', 7, 'BQlv3u7AIVW', 27);
INSERT INTO train_booking VALUES (28, '2010-03-08', 8, 'Kn', 28);
INSERT INTO train_booking VALUES (29, '2011-09-19', 9, 'JOdPMoK1NPCERyiQht', 29);
INSERT INTO train_booking VALUES (30, '2000-10-01', 10, 'eID3TC', 30);
INSERT INTO train_booking VALUES (31, '2018-02-15', 11, 'weBYrHO4OKPySLbXhlN', 31);
INSERT INTO train_booking VALUES (32, '2002-12-12', 12, 'PMrovuaTHLvyRl3f', 32);
INSERT INTO train_booking VALUES (33, '2009-05-23', 13, 'U', 33);
INSERT INTO train_booking VALUES (34, '2000-07-15', 14, 'zij', 34);
INSERT INTO train_booking VALUES (35, '2003-03-21', 15, 'ZHHAshjVN', 35);
INSERT INTO train_booking VALUES (36, '2015-05-02', 16, 'V5', 36);
INSERT INTO train_booking VALUES (37, '2013-04-24', 17, 'kwPVXr', 37);
INSERT INTO train_booking VALUES (38, '2015-02-19', 18, 'lvFWd4jgXK', 38);
INSERT INTO train_booking VALUES (39, '2008-10-31', 19, 'ldKjhHO0g', 39);
INSERT INTO train_booking VALUES (40, '2005-08-09', 20, 'R2mbef3xoyEBwhRh0Dbw', 40);
INSERT INTO train_booking VALUES (41, '2018-03-19', 1, 'vWJ5EZp58mIbW8h4', 41);
INSERT INTO train_booking VALUES (42, '2006-01-13', 2, 'C8qp8jtTbwy6lX4eFzk', 42);
INSERT INTO train_booking VALUES (43, '2012-06-08', 3, 'bBrzxxy5eK3N4p20', 43);
INSERT INTO train_booking VALUES (44, '2012-02-15', 4, 'ez5ZuDUjVp4', 44);
INSERT INTO train_booking VALUES (45, '2017-06-09', 5, 'Pz740', 45);
INSERT INTO train_booking VALUES (46, '2005-09-17', 6, 'AqU0GcTf4SVD', 46);
INSERT INTO train_booking VALUES (47, '2005-08-25', 7, 'D6h44K77iERheg4TI', 47);
INSERT INTO train_booking VALUES (48, '2011-09-26', 8, 'xFQGNEmj8x', 48);
INSERT INTO train_booking VALUES (49, '2000-06-20', 9, 'uHx8Na2im62ox', 49);
INSERT INTO train_booking VALUES (50, '2013-09-08', 10, 'kI0iqCHW6zqnbiW3mm', 50);
INSERT INTO train_booking VALUES (17, '2002-12-26', 17, 'aiwfdnKNAWF', 17);
INSERT INTO train_booking VALUES (6, '2013-03-29', 6, 'ABCD', 6);
INSERT INTO train_booking VALUES (51, '2018-05-27', 21, '1', 26);
INSERT INTO train_booking VALUES (52, '2018-05-27', 21, '3', 26);
INSERT INTO train_booking VALUES (53, '2018-05-31', 21, '1234', 26);
INSERT INTO train_booking VALUES (54, '2018-05-31', 1, '0987', 26);


--
-- TOC entry 2794 (class 2606 OID 50664)
-- Name: train_booking train_booking_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_booking
    ADD CONSTRAINT train_booking_pkey PRIMARY KEY (booking_id);


--
-- TOC entry 2797 (class 2606 OID 50675)
-- Name: train_booking train_booking_booked_by_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_booking
    ADD CONSTRAINT train_booking_booked_by_fkey FOREIGN KEY (booked_by) REFERENCES customer(customer_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2796 (class 2606 OID 50670)
-- Name: train_booking train_booking_passenger_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_booking
    ADD CONSTRAINT train_booking_passenger_id_fkey FOREIGN KEY (passenger_id) REFERENCES passenger(id_card) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2795 (class 2606 OID 50665)
-- Name: train_booking train_booking_schedule_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_booking
    ADD CONSTRAINT train_booking_schedule_id_fkey FOREIGN KEY (schedule_id) REFERENCES train_schedule(schedule_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:13:14

--
-- PostgreSQL database dump complete
--

