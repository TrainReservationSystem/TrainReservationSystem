--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:15:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 234 (class 1259 OID 50587)
-- Name: food_order; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE food_order (
    food_order_id integer DEFAULT nextval('train_foodorder_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    customer_id integer NOT NULL,
    food_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE food_order OWNER TO postgres;

--
-- TOC entry 2918 (class 0 OID 50587)
-- Dependencies: 234
-- Data for Name: food_order; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO food_order VALUES (1, '2009-05-30', 1, 25, 600739);
INSERT INTO food_order VALUES (2, '2014-07-11', 2, 24, 95872);
INSERT INTO food_order VALUES (3, '2009-02-07', 3, 23, 478282);
INSERT INTO food_order VALUES (4, '2006-09-23', 4, 22, 371390);
INSERT INTO food_order VALUES (5, '2009-06-18', 5, 21, 175161);
INSERT INTO food_order VALUES (6, '2011-03-14', 6, 15, 344061);
INSERT INTO food_order VALUES (7, '2003-09-25', 7, 14, 537137);
INSERT INTO food_order VALUES (8, '2016-02-13', 8, 13, 999872);
INSERT INTO food_order VALUES (9, '2014-05-08', 9, 12, 961631);
INSERT INTO food_order VALUES (10, '2014-03-19', 10, 11, 272035);
INSERT INTO food_order VALUES (11, '2017-06-21', 11, 5, 617303);
INSERT INTO food_order VALUES (12, '2011-03-08', 12, 4, 361093);
INSERT INTO food_order VALUES (13, '2002-04-30', 13, 3, 209382);
INSERT INTO food_order VALUES (14, '2003-05-31', 14, 2, 830495);
INSERT INTO food_order VALUES (15, '2006-11-12', 15, 1, 640704);
INSERT INTO food_order VALUES (16, '2015-10-19', 16, 30, 693366);
INSERT INTO food_order VALUES (17, '2005-10-07', 17, 29, 250584);
INSERT INTO food_order VALUES (18, '2001-04-25', 18, 28, 307420);
INSERT INTO food_order VALUES (19, '2012-12-13', 19, 27, 100858);
INSERT INTO food_order VALUES (20, '2009-06-17', 20, 26, 21227);
INSERT INTO food_order VALUES (21, '2009-11-23', 21, 25, 93893);
INSERT INTO food_order VALUES (22, '2015-07-14', 22, 24, 590205);
INSERT INTO food_order VALUES (23, '2017-11-24', 23, 23, 866299);
INSERT INTO food_order VALUES (24, '2000-07-23', 24, 22, 485590);
INSERT INTO food_order VALUES (25, '2002-02-22', 25, 21, 373321);
INSERT INTO food_order VALUES (26, '2014-12-28', 26, 30, 833877);
INSERT INTO food_order VALUES (27, '2011-01-02', 27, 29, 987088);
INSERT INTO food_order VALUES (28, '2005-09-10', 28, 28, 871057);
INSERT INTO food_order VALUES (29, '2011-01-02', 29, 27, 233489);
INSERT INTO food_order VALUES (30, '2013-09-11', 30, 26, 245689);
INSERT INTO food_order VALUES (31, '2017-09-27', 31, 15, 380915);
INSERT INTO food_order VALUES (32, '2004-04-28', 32, 14, 664486);
INSERT INTO food_order VALUES (33, '2005-03-13', 33, 13, 479908);
INSERT INTO food_order VALUES (34, '2011-06-05', 34, 12, 576228);
INSERT INTO food_order VALUES (35, '2012-07-31', 35, 11, 545106);
INSERT INTO food_order VALUES (36, '2015-12-25', 36, 20, 71668);
INSERT INTO food_order VALUES (37, '2017-01-31', 37, 19, 373084);
INSERT INTO food_order VALUES (38, '2001-09-08', 38, 18, 431585);
INSERT INTO food_order VALUES (39, '2017-08-02', 39, 17, 8301);
INSERT INTO food_order VALUES (40, '2010-03-22', 40, 16, 960290);
INSERT INTO food_order VALUES (41, '2006-09-22', 41, 5, 630094);
INSERT INTO food_order VALUES (42, '2013-12-04', 42, 4, 354969);
INSERT INTO food_order VALUES (43, '2015-08-31', 43, 3, 264497);
INSERT INTO food_order VALUES (44, '2010-03-14', 44, 2, 945904);
INSERT INTO food_order VALUES (45, '2012-03-01', 45, 1, 140330);
INSERT INTO food_order VALUES (46, '2006-03-01', 46, 10, 427061);
INSERT INTO food_order VALUES (47, '2015-03-23', 47, 9, 230083);
INSERT INTO food_order VALUES (48, '2007-01-29', 48, 8, 853139);
INSERT INTO food_order VALUES (49, '2015-04-18', 49, 7, 367183);
INSERT INTO food_order VALUES (50, '2010-04-08', 50, 6, 681114);
INSERT INTO food_order VALUES (51, '2018-06-02', 26, 28, 1);
INSERT INTO food_order VALUES (52, '2018-06-02', 26, 28, 2);


--
-- TOC entry 2794 (class 2606 OID 50591)
-- Name: food_order food_order_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY food_order
    ADD CONSTRAINT food_order_pkey PRIMARY KEY (food_order_id);


--
-- TOC entry 2795 (class 2606 OID 50592)
-- Name: food_order food_order_customer_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY food_order
    ADD CONSTRAINT food_order_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(customer_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2796 (class 2606 OID 50597)
-- Name: food_order food_order_food_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY food_order
    ADD CONSTRAINT food_order_food_id_fkey FOREIGN KEY (food_id) REFERENCES food(food_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:15:52

--
-- PostgreSQL database dump complete
--

