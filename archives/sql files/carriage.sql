--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:16:58

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 226 (class 1259 OID 50467)
-- Name: carriage; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE carriage (
    carriage_id integer NOT NULL,
    carriage_name character varying(20) NOT NULL,
    train_id integer NOT NULL
);


ALTER TABLE carriage OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 50467)
-- Dependencies: 226
-- Data for Name: carriage; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO carriage VALUES (1, 'CTomiQutSiSyLt', 1);
INSERT INTO carriage VALUES (2, 'g5', 1);
INSERT INTO carriage VALUES (3, 'Lo', 1);
INSERT INTO carriage VALUES (4, 'WQIh8tT0Ke2', 1);
INSERT INTO carriage VALUES (5, '586Qtq86', 1);
INSERT INTO carriage VALUES (6, 'xmLqEfigdFLb6E', 1);
INSERT INTO carriage VALUES (7, 'EcBUW', 1);
INSERT INTO carriage VALUES (8, 'Of', 1);
INSERT INTO carriage VALUES (9, 'N0oJY', 1);
INSERT INTO carriage VALUES (10, 'Svvdgw5On5mbUgumJvE', 1);
INSERT INTO carriage VALUES (11, 'qxaVQyxH2Mzah3bw1X', 2);
INSERT INTO carriage VALUES (12, 'yOINGe', 2);
INSERT INTO carriage VALUES (13, 'SWcNv6r6oE3', 2);
INSERT INTO carriage VALUES (14, 'pEJO7ZwIjq7HWFk', 2);
INSERT INTO carriage VALUES (15, 'BaOKa31', 2);
INSERT INTO carriage VALUES (16, '7GsilyBVMYcPuP1Q', 2);
INSERT INTO carriage VALUES (17, '0XMhLJzPOLFr5fQGQx5', 2);
INSERT INTO carriage VALUES (18, 'SSbeZYIhigp', 2);
INSERT INTO carriage VALUES (19, 'BON13XpY', 2);
INSERT INTO carriage VALUES (20, 'vRMJi7I6XqzPujZi', 2);
INSERT INTO carriage VALUES (21, 'Y0qZ1AdHGR2', 3);
INSERT INTO carriage VALUES (22, 'frOclP3m', 3);
INSERT INTO carriage VALUES (23, '7U4cjETkMJmv', 3);
INSERT INTO carriage VALUES (24, 'L', 3);
INSERT INTO carriage VALUES (25, 'ku4GkR3', 3);
INSERT INTO carriage VALUES (26, 'eYjTIRMQ4WGyBj8Qz', 3);
INSERT INTO carriage VALUES (27, 'QN8IjUKAe', 3);
INSERT INTO carriage VALUES (28, '2tyJz8xT7F7GcCTkp', 3);
INSERT INTO carriage VALUES (29, '817s', 3);
INSERT INTO carriage VALUES (30, 'zNcthUlJ3e', 3);
INSERT INTO carriage VALUES (31, 'r3KZJpC6MjWdxZfDGm', 4);
INSERT INTO carriage VALUES (32, 'T', 4);
INSERT INTO carriage VALUES (33, 'l2', 4);
INSERT INTO carriage VALUES (34, 'H', 4);
INSERT INTO carriage VALUES (35, 'lykdkycdCVj0yV4', 4);
INSERT INTO carriage VALUES (36, 'MP', 4);
INSERT INTO carriage VALUES (37, 'XKB70jCF', 4);
INSERT INTO carriage VALUES (38, 'SgMUYpOJi4dkwomf2f', 4);
INSERT INTO carriage VALUES (39, 'czP2WTvEOZ3zNjoDD', 4);
INSERT INTO carriage VALUES (40, 'P', 4);
INSERT INTO carriage VALUES (41, 'SvXAx87iyJIY18', 5);
INSERT INTO carriage VALUES (42, 'XnRfSuEv56y7YDX', 5);
INSERT INTO carriage VALUES (43, 'MRZ6', 5);
INSERT INTO carriage VALUES (44, 'aAcdmGj0Zw1vNqOeslBz', 5);
INSERT INTO carriage VALUES (45, 'IFN1xSBmfHrou', 5);
INSERT INTO carriage VALUES (46, 'EuBxUj', 5);
INSERT INTO carriage VALUES (47, 'M2488BxPTe', 5);
INSERT INTO carriage VALUES (48, 'UXKct1NyM8I1l', 5);
INSERT INTO carriage VALUES (49, 'bxWgl2SE', 5);
INSERT INTO carriage VALUES (50, 'GlXnGIwsBUzuGVe', 5);
INSERT INTO carriage VALUES (51, 'iMFy0TU37Dx', 6);
INSERT INTO carriage VALUES (52, 'LCmcDF5L7I4Wt7IRm', 6);
INSERT INTO carriage VALUES (53, 'A', 6);
INSERT INTO carriage VALUES (54, 'V', 6);
INSERT INTO carriage VALUES (55, 'v7avHkziSXLm3LefC', 6);
INSERT INTO carriage VALUES (56, 'a7fTrga3f2', 6);
INSERT INTO carriage VALUES (57, 'lYoygsMYCb7', 6);
INSERT INTO carriage VALUES (58, 'F2ZYkwZuMX288nXWrmp', 6);
INSERT INTO carriage VALUES (59, '3Dg4FqIcpA2IH', 6);
INSERT INTO carriage VALUES (60, 'QieBF', 6);
INSERT INTO carriage VALUES (61, 'TzRpgipaXZ', 7);
INSERT INTO carriage VALUES (62, '1MuOUpCxRih', 7);
INSERT INTO carriage VALUES (63, 'nQFOsgIkMpm', 7);
INSERT INTO carriage VALUES (64, '0EM', 7);
INSERT INTO carriage VALUES (65, '8LeVhdW', 7);
INSERT INTO carriage VALUES (66, '66eNH', 7);
INSERT INTO carriage VALUES (67, 'g', 7);
INSERT INTO carriage VALUES (68, '0nu5M', 7);
INSERT INTO carriage VALUES (69, 'ObIiehwVTgt3hvVx54a', 7);
INSERT INTO carriage VALUES (70, 'e8DyjtfukkOZzjJ', 7);
INSERT INTO carriage VALUES (71, 'DskKq', 8);
INSERT INTO carriage VALUES (72, 'T6nBIZKySWUBdNxdcYIA', 8);
INSERT INTO carriage VALUES (73, 'W0LvT', 8);
INSERT INTO carriage VALUES (74, 'y1atoqcr', 8);
INSERT INTO carriage VALUES (75, 'Pnky0pOhcmD6I04F', 8);
INSERT INTO carriage VALUES (76, '7Y', 8);
INSERT INTO carriage VALUES (77, 'ldr', 8);
INSERT INTO carriage VALUES (78, 'wWa75dtUKPdpy4xe4', 8);
INSERT INTO carriage VALUES (79, 'eYo1cg8hKf5r', 8);
INSERT INTO carriage VALUES (80, 'TcnkkASO', 8);
INSERT INTO carriage VALUES (81, 'vLm', 9);
INSERT INTO carriage VALUES (82, 'D', 9);
INSERT INTO carriage VALUES (83, 'IxO1L3W1FgLzcD', 9);
INSERT INTO carriage VALUES (84, 'jd', 9);
INSERT INTO carriage VALUES (85, 'CCi4DfmhBTezMYL60j', 9);
INSERT INTO carriage VALUES (86, 'rda1l', 9);
INSERT INTO carriage VALUES (87, 'SyW2Ae', 9);
INSERT INTO carriage VALUES (88, '0kbs7V0ROOb', 9);
INSERT INTO carriage VALUES (89, 'pVnb', 9);
INSERT INTO carriage VALUES (90, 'M', 9);
INSERT INTO carriage VALUES (91, '8e0gBS', 10);
INSERT INTO carriage VALUES (92, 'tLX11ZO8TjYOX', 10);
INSERT INTO carriage VALUES (93, 'lYMwF4O', 10);
INSERT INTO carriage VALUES (94, 'WGZnDjK31voKFl5ltUV', 10);
INSERT INTO carriage VALUES (95, 'SXeTqHWkE', 10);
INSERT INTO carriage VALUES (96, '04AKDsgXUwrF5iCj', 10);
INSERT INTO carriage VALUES (97, 'WS5gAUS5OdGEfaX', 10);
INSERT INTO carriage VALUES (98, 'Dx1kchUMnWnu6YKAg', 10);
INSERT INTO carriage VALUES (99, '5pB8bBov6', 10);
INSERT INTO carriage VALUES (100, 'k6TRRlYsu', 10);


--
-- TOC entry 2793 (class 2606 OID 50471)
-- Name: carriage carriage_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY carriage
    ADD CONSTRAINT carriage_pkey PRIMARY KEY (carriage_id);


--
-- TOC entry 2794 (class 2606 OID 50472)
-- Name: carriage carriage_train_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY carriage
    ADD CONSTRAINT carriage_train_id_fkey FOREIGN KEY (train_id) REFERENCES train(train_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:16:58

--
-- PostgreSQL database dump complete
--

