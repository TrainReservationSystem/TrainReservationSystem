--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:16:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 232 (class 1259 OID 50572)
-- Name: food_category; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE food_category (
    food_category_id integer NOT NULL,
    food_category_name character varying(20) NOT NULL
);


ALTER TABLE food_category OWNER TO postgres;

--
-- TOC entry 2915 (class 0 OID 50572)
-- Dependencies: 232
-- Data for Name: food_category; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO food_category VALUES (1, 'drink');
INSERT INTO food_category VALUES (2, 'meal');
INSERT INTO food_category VALUES (3, 'snack');


--
-- TOC entry 2793 (class 2606 OID 50576)
-- Name: food_category food_category_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY food_category
    ADD CONSTRAINT food_category_pkey PRIMARY KEY (food_category_id);


-- Completed on 2018-06-02 16:16:09

--
-- PostgreSQL database dump complete
--

