--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:12:56

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 222 (class 1259 OID 50427)
-- Name: train_conductor; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE train_conductor (
    conductor_id integer NOT NULL,
    username character varying(20) NOT NULL
);


ALTER TABLE train_conductor OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 50427)
-- Dependencies: 222
-- Data for Name: train_conductor; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO train_conductor VALUES (1, 'Carlos');
INSERT INTO train_conductor VALUES (2, 'Edwyn01');
INSERT INTO train_conductor VALUES (3, 'Cloe694');
INSERT INTO train_conductor VALUES (4, 'Tyler200');
INSERT INTO train_conductor VALUES (5, 'Thomas5');
INSERT INTO train_conductor VALUES (6, 'Dave976');
INSERT INTO train_conductor VALUES (7, 'Eleanor');
INSERT INTO train_conductor VALUES (8, 'Piet');
INSERT INTO train_conductor VALUES (9, 'Thomas');
INSERT INTO train_conductor VALUES (10, 'Herb47');
INSERT INTO train_conductor VALUES (11, 'GertJan342');
INSERT INTO train_conductor VALUES (12, 'Alexander810');
INSERT INTO train_conductor VALUES (13, 'Martin');
INSERT INTO train_conductor VALUES (14, 'Scottie');
INSERT INTO train_conductor VALUES (15, 'Matthew977');
INSERT INTO train_conductor VALUES (16, 'Charlotte619');
INSERT INTO train_conductor VALUES (17, 'Cees72');
INSERT INTO train_conductor VALUES (18, 'Guus3');
INSERT INTO train_conductor VALUES (19, 'Oliver609');
INSERT INTO train_conductor VALUES (20, 'Cath');
INSERT INTO train_conductor VALUES (21, 'Niklas109');
INSERT INTO train_conductor VALUES (22, 'Tommy514');
INSERT INTO train_conductor VALUES (23, 'Maja9');
INSERT INTO train_conductor VALUES (24, 'Sem8');
INSERT INTO train_conductor VALUES (25, 'Ike990');
INSERT INTO train_conductor VALUES (26, 'Giel');
INSERT INTO train_conductor VALUES (27, 'Linnea3');
INSERT INTO train_conductor VALUES (28, 'Robert61');
INSERT INTO train_conductor VALUES (29, 'Lukas45');
INSERT INTO train_conductor VALUES (30, 'Nico362');


--
-- TOC entry 2793 (class 2606 OID 50431)
-- Name: train_conductor conductor_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_conductor
    ADD CONSTRAINT conductor_pkey PRIMARY KEY (conductor_id);


--
-- TOC entry 2794 (class 2606 OID 50432)
-- Name: train_conductor conductor_username_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_conductor
    ADD CONSTRAINT conductor_username_fkey FOREIGN KEY (username) REFERENCES user_(username) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:12:56

--
-- PostgreSQL database dump complete
--

