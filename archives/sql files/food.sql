--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:16:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 233 (class 1259 OID 50577)
-- Name: food; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE food (
    food_id integer NOT NULL,
    food_name character varying(20) NOT NULL,
    price double precision NOT NULL,
    food_category_id integer NOT NULL
);


ALTER TABLE food OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 50577)
-- Dependencies: 233
-- Data for Name: food; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO food VALUES (1, 'Mission burrito', 4803.54, 2);
INSERT INTO food VALUES (2, 'Scampi', 6.4900000000000002, 2);
INSERT INTO food VALUES (3, 'Bacon', 145.38999999999999, 2);
INSERT INTO food VALUES (4, 'Cheese steak', 6.7800000000000002, 2);
INSERT INTO food VALUES (5, 'Frybread', 551.76999999999998, 1);
INSERT INTO food VALUES (6, 'Fajita', 93.900000000000006, 1);
INSERT INTO food VALUES (7, 'Chili dog', 108.31, 1);
INSERT INTO food VALUES (8, 'Senate bean soup', 1790.2, 2);
INSERT INTO food VALUES (9, 'Choco pie', 23092.540000000001, 1);
INSERT INTO food VALUES (10, 'Sandwich', 8.8599999999999994, 1);
INSERT INTO food VALUES (11, 'Celery Victor', 4922.8800000000001, 2);
INSERT INTO food VALUES (12, 'Onion ring', 680.55999999999995, 1);
INSERT INTO food VALUES (13, 'Vanilla Milkshake', 35, 3);
INSERT INTO food VALUES (14, 'Hot Chocolate', 17.41, 3);
INSERT INTO food VALUES (15, 'Ice Tea', 2.6699999999999999, 3);
INSERT INTO food VALUES (16, 'Stuffed peppers', 95661.5, 1);
INSERT INTO food VALUES (17, 'Maple bacon donut', 735.63999999999999, 1);
INSERT INTO food VALUES (18, 'Mashed pumpkin', 74.709999999999994, 2);
INSERT INTO food VALUES (19, 'Chinese cuisine', 5546, 2);
INSERT INTO food VALUES (20, 'Luwak Coffee', 3310.1999999999998, 3);
INSERT INTO food VALUES (21, 'Clam chowder', 555.32000000000005, 2);
INSERT INTO food VALUES (22, 'Ice cream cake', 4.8799999999999999, 1);
INSERT INTO food VALUES (23, 'Brandy', 94127.889999999999, 3);
INSERT INTO food VALUES (24, 'Pizza strips', 108.58, 2);
INSERT INTO food VALUES (25, 'Orange Juice', 62.619999999999997, 3);
INSERT INTO food VALUES (26, 'Reuben sandwich', 3.0699999999999998, 2);
INSERT INTO food VALUES (27, 'Bread', 5.9500000000000002, 1);
INSERT INTO food VALUES (28, 'Peanut butter', 0.87, 1);
INSERT INTO food VALUES (29, 'Chicken stew', 1.8899999999999999, 2);
INSERT INTO food VALUES (30, 'Waffles', 844.63, 1);


--
-- TOC entry 2793 (class 2606 OID 50581)
-- Name: food food_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY food
    ADD CONSTRAINT food_pkey PRIMARY KEY (food_id);


--
-- TOC entry 2794 (class 2606 OID 50582)
-- Name: food food_food_category_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY food
    ADD CONSTRAINT food_food_category_id_fkey FOREIGN KEY (food_category_id) REFERENCES food_category(food_category_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:16:20

--
-- PostgreSQL database dump complete
--

