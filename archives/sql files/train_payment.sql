--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:12:41

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 237 (class 1259 OID 50680)
-- Name: train_payment; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE train_payment (
    train_payment_id integer NOT NULL,
    payment_date date NOT NULL,
    total_payment double precision NOT NULL,
    status character varying(10) NOT NULL,
    customer_id integer NOT NULL,
    booking_id integer NOT NULL,
    CONSTRAINT chk_status CHECK ((((status)::text = 'paid'::text) OR ((status)::text = 'not paid'::text)))
);


ALTER TABLE train_payment OWNER TO postgres;

--
-- TOC entry 2918 (class 0 OID 50680)
-- Dependencies: 237
-- Data for Name: train_payment; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO train_payment VALUES (1, '2000-07-14', 73357.869999999995, 'paid', 1, 1);
INSERT INTO train_payment VALUES (2, '2007-11-11', 845.94000000000005, 'not paid', 2, 2);
INSERT INTO train_payment VALUES (3, '2009-04-13', 3.8900000000000001, 'not paid', 3, 3);
INSERT INTO train_payment VALUES (4, '2015-04-23', 37.210000000000001, 'paid', 4, 4);
INSERT INTO train_payment VALUES (5, '2008-03-07', 778.89999999999998, 'paid', 5, 5);
INSERT INTO train_payment VALUES (6, '2016-02-27', 29477.529999999999, 'paid', 6, 6);
INSERT INTO train_payment VALUES (7, '2011-07-13', 924464.51000000001, 'paid', 7, 7);
INSERT INTO train_payment VALUES (8, '2013-03-17', 7.3300000000000001, 'paid', 8, 8);
INSERT INTO train_payment VALUES (9, '2015-08-27', 9.8100000000000005, 'paid', 9, 9);
INSERT INTO train_payment VALUES (10, '2013-12-01', 2.6699999999999999, 'not paid', 10, 10);
INSERT INTO train_payment VALUES (11, '2000-09-02', 600.37, 'paid', 11, 11);
INSERT INTO train_payment VALUES (12, '2001-01-23', 60.729999999999997, 'paid', 12, 12);
INSERT INTO train_payment VALUES (13, '2006-11-27', 70439.839999999997, 'not paid', 13, 13);
INSERT INTO train_payment VALUES (14, '2017-06-05', 3.4100000000000001, 'paid', 14, 14);
INSERT INTO train_payment VALUES (15, '2014-04-14', 74516.690000000002, 'not paid', 15, 15);
INSERT INTO train_payment VALUES (16, '2013-10-18', 19686.990000000002, 'paid', 16, 16);
INSERT INTO train_payment VALUES (17, '2000-09-13', 8866.2099999999991, 'paid', 17, 17);
INSERT INTO train_payment VALUES (18, '2015-04-25', 6.4299999999999997, 'paid', 18, 18);
INSERT INTO train_payment VALUES (19, '2016-11-05', 70.379999999999995, 'paid', 19, 19);
INSERT INTO train_payment VALUES (20, '2011-09-02', 40.740000000000002, 'paid', 20, 20);
INSERT INTO train_payment VALUES (21, '2010-06-27', 25.760000000000002, 'paid', 21, 21);
INSERT INTO train_payment VALUES (22, '2003-01-14', 903989.72999999998, 'paid', 22, 22);
INSERT INTO train_payment VALUES (23, '2000-02-15', 794353.30000000005, 'paid', 23, 23);
INSERT INTO train_payment VALUES (24, '2004-01-29', 17557.990000000002, 'paid', 24, 24);
INSERT INTO train_payment VALUES (25, '2004-07-28', 660613.88, 'paid', 25, 25);
INSERT INTO train_payment VALUES (26, '2007-09-16', 15.359999999999999, 'paid', 26, 26);
INSERT INTO train_payment VALUES (27, '2016-11-07', 65817.970000000001, 'paid', 27, 27);
INSERT INTO train_payment VALUES (28, '2011-04-24', 303.63999999999999, 'paid', 28, 28);
INSERT INTO train_payment VALUES (29, '2006-01-15', 896129.16000000003, 'not paid', 29, 29);
INSERT INTO train_payment VALUES (30, '2006-09-04', 94457.050000000003, 'paid', 30, 30);
INSERT INTO train_payment VALUES (31, '2015-07-21', 1944.6900000000001, 'not paid', 31, 31);
INSERT INTO train_payment VALUES (32, '2000-06-09', 98.420000000000002, 'paid', 32, 32);
INSERT INTO train_payment VALUES (33, '2010-02-28', 32450.740000000002, 'paid', 33, 33);
INSERT INTO train_payment VALUES (34, '2011-08-19', 3.71, 'paid', 34, 34);
INSERT INTO train_payment VALUES (35, '2016-06-24', 9026.7399999999998, 'paid', 35, 35);
INSERT INTO train_payment VALUES (36, '2014-05-29', 76.420000000000002, 'paid', 36, 36);
INSERT INTO train_payment VALUES (37, '2014-03-21', 879.07000000000005, 'paid', 37, 37);
INSERT INTO train_payment VALUES (38, '2001-10-16', 337.14999999999998, 'paid', 38, 38);
INSERT INTO train_payment VALUES (39, '2017-06-17', 1.01, 'paid', 39, 39);
INSERT INTO train_payment VALUES (40, '2004-10-17', 814.71000000000004, 'paid', 40, 40);
INSERT INTO train_payment VALUES (41, '2015-02-05', 93.129999999999995, 'paid', 41, 41);
INSERT INTO train_payment VALUES (42, '2002-01-27', 60.969999999999999, 'paid', 42, 42);
INSERT INTO train_payment VALUES (43, '2015-05-24', 4923.0600000000004, 'paid', 43, 43);
INSERT INTO train_payment VALUES (44, '2017-01-02', 7277.4300000000003, 'paid', 44, 44);
INSERT INTO train_payment VALUES (45, '2016-08-03', 56.920000000000002, 'paid', 45, 45);
INSERT INTO train_payment VALUES (46, '2012-08-27', 829852.29000000004, 'paid', 46, 46);
INSERT INTO train_payment VALUES (47, '2007-08-04', 8.9600000000000009, 'paid', 47, 47);
INSERT INTO train_payment VALUES (48, '2011-09-16', 24057.099999999999, 'paid', 48, 48);
INSERT INTO train_payment VALUES (49, '2017-01-18', 5.5800000000000001, 'paid', 49, 49);
INSERT INTO train_payment VALUES (50, '2001-09-11', 37.609999999999999, 'not paid', 50, 50);


--
-- TOC entry 2794 (class 2606 OID 50685)
-- Name: train_payment train_payment_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_payment
    ADD CONSTRAINT train_payment_pkey PRIMARY KEY (train_payment_id);


--
-- TOC entry 2796 (class 2606 OID 50691)
-- Name: train_payment train_payment_booking_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_payment
    ADD CONSTRAINT train_payment_booking_id_fkey FOREIGN KEY (booking_id) REFERENCES train_booking(booking_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2795 (class 2606 OID 50686)
-- Name: train_payment train_payment_customer_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_payment
    ADD CONSTRAINT train_payment_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(customer_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:12:41

--
-- PostgreSQL database dump complete
--

