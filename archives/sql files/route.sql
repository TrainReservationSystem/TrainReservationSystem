--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:14:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 224 (class 1259 OID 50447)
-- Name: route; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE route (
    route_id integer NOT NULL,
    number_of_stop integer NOT NULL,
    origin_station integer,
    destination_station integer
);


ALTER TABLE route OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 50447)
-- Dependencies: 224
-- Data for Name: route; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO route VALUES (20, 10, 10, 5);
INSERT INTO route VALUES (12, 5, 6, 2);
INSERT INTO route VALUES (11, 29, 6, 1);
INSERT INTO route VALUES (14, 25, 7, 4);
INSERT INTO route VALUES (13, 18, 7, 3);
INSERT INTO route VALUES (16, 17, 8, 1);
INSERT INTO route VALUES (15, 15, 8, 5);
INSERT INTO route VALUES (18, 30, 9, 3);
INSERT INTO route VALUES (17, 15, 9, 2);
INSERT INTO route VALUES (19, 28, 10, 4);
INSERT INTO route VALUES (2, 27, 1, 7);
INSERT INTO route VALUES (1, 23, 1, 6);
INSERT INTO route VALUES (4, 10, 2, 9);
INSERT INTO route VALUES (3, 4, 2, 8);
INSERT INTO route VALUES (6, 3, 3, 6);
INSERT INTO route VALUES (5, 19, 3, 10);
INSERT INTO route VALUES (8, 30, 4, 8);
INSERT INTO route VALUES (7, 11, 4, 7);
INSERT INTO route VALUES (10, 20, 5, 10);
INSERT INTO route VALUES (9, 8, 5, 9);


--
-- TOC entry 2793 (class 2606 OID 50451)
-- Name: route route_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route_pkey PRIMARY KEY (route_id);


--
-- TOC entry 2795 (class 2606 OID 50713)
-- Name: route route_destination_station_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route_destination_station_fkey FOREIGN KEY (destination_station) REFERENCES station(station_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2794 (class 2606 OID 50708)
-- Name: route route_origin_station_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route_origin_station_fkey FOREIGN KEY (origin_station) REFERENCES station(station_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:14:52

--
-- PostgreSQL database dump complete
--

