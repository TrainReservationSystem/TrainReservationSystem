--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:15:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 238 (class 1259 OID 50696)
-- Name: payment; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE payment (
    payment_id integer NOT NULL,
    payment_date date NOT NULL,
    total_payment double precision NOT NULL,
    status character varying(10) NOT NULL,
    customer_id integer NOT NULL,
    CONSTRAINT chk_status CHECK ((((status)::text = 'paid'::text) OR ((status)::text = 'not paid'::text)))
);


ALTER TABLE payment OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 50696)
-- Dependencies: 238
-- Data for Name: payment; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO payment VALUES (1, '2012-08-23', 8041.6599999999999, 'paid', 1);
INSERT INTO payment VALUES (2, '2009-01-05', 96079.529999999999, 'not paid', 2);
INSERT INTO payment VALUES (3, '2008-02-15', 55521.050000000003, 'paid', 3);
INSERT INTO payment VALUES (4, '2009-09-26', 47511.75, 'paid', 4);
INSERT INTO payment VALUES (5, '2009-07-19', 6769.9300000000003, 'paid', 5);
INSERT INTO payment VALUES (6, '2013-09-12', 274.88999999999999, 'paid', 6);
INSERT INTO payment VALUES (7, '2002-05-30', 19962.849999999999, 'paid', 7);
INSERT INTO payment VALUES (8, '2003-05-12', 742.64999999999998, 'not paid', 8);
INSERT INTO payment VALUES (9, '2013-02-16', 32.119999999999997, 'paid', 9);
INSERT INTO payment VALUES (10, '2007-06-09', 960699.71999999997, 'not paid', 10);
INSERT INTO payment VALUES (11, '2011-03-14', 3146.6100000000001, 'paid', 11);
INSERT INTO payment VALUES (12, '2000-11-22', 987735.38, 'paid', 12);
INSERT INTO payment VALUES (13, '2010-05-07', 39.75, 'not paid', 13);
INSERT INTO payment VALUES (14, '2014-06-25', 5.5499999999999998, 'paid', 14);
INSERT INTO payment VALUES (15, '2000-06-08', 6475.1999999999998, 'paid', 15);
INSERT INTO payment VALUES (16, '2016-10-22', 468.69999999999999, 'paid', 16);
INSERT INTO payment VALUES (17, '2004-08-31', 23.379999999999999, 'paid', 17);
INSERT INTO payment VALUES (18, '2003-04-15', 5581.7399999999998, 'paid', 18);
INSERT INTO payment VALUES (19, '2013-11-20', 7428.8999999999996, 'paid', 19);
INSERT INTO payment VALUES (20, '2008-12-31', 7580.6800000000003, 'paid', 20);
INSERT INTO payment VALUES (21, '2015-03-30', 85912.259999999995, 'paid', 21);
INSERT INTO payment VALUES (22, '2017-07-20', 9.1699999999999999, 'paid', 22);
INSERT INTO payment VALUES (23, '2006-08-25', 477.80000000000001, 'not paid', 23);
INSERT INTO payment VALUES (24, '2015-12-14', 40.380000000000003, 'paid', 24);
INSERT INTO payment VALUES (25, '2004-09-04', 46663.040000000001, 'paid', 25);
INSERT INTO payment VALUES (26, '2003-03-06', 113.77, 'paid', 26);
INSERT INTO payment VALUES (27, '2009-04-13', 478803.92999999999, 'paid', 27);
INSERT INTO payment VALUES (28, '2013-06-26', 9336.2600000000002, 'paid', 28);
INSERT INTO payment VALUES (29, '2012-10-28', 3307.4400000000001, 'not paid', 29);
INSERT INTO payment VALUES (30, '2011-07-29', 9363.7399999999998, 'paid', 30);
INSERT INTO payment VALUES (31, '2008-01-29', 4.6200000000000001, 'paid', 31);
INSERT INTO payment VALUES (32, '2014-11-19', 14, 'not paid', 32);
INSERT INTO payment VALUES (33, '2014-01-08', 5868.5900000000001, 'not paid', 33);
INSERT INTO payment VALUES (34, '2000-10-12', 4156.6499999999996, 'paid', 34);
INSERT INTO payment VALUES (35, '2016-06-19', 83184.990000000005, 'paid', 35);
INSERT INTO payment VALUES (36, '2006-01-04', 3967, 'paid', 36);
INSERT INTO payment VALUES (37, '2011-12-17', 38.939999999999998, 'paid', 37);
INSERT INTO payment VALUES (38, '2004-01-16', 56.020000000000003, 'paid', 38);
INSERT INTO payment VALUES (39, '2017-01-08', 665608.68000000005, 'paid', 39);
INSERT INTO payment VALUES (40, '2002-09-24', 85.719999999999999, 'paid', 40);
INSERT INTO payment VALUES (41, '2013-03-02', 7.4900000000000002, 'paid', 41);
INSERT INTO payment VALUES (42, '2002-10-29', 96.629999999999995, 'paid', 42);
INSERT INTO payment VALUES (43, '2005-01-05', 31.030000000000001, 'paid', 43);
INSERT INTO payment VALUES (44, '2008-07-21', 7, 'paid', 44);
INSERT INTO payment VALUES (45, '2018-02-24', 95.25, 'not paid', 45);
INSERT INTO payment VALUES (46, '2004-11-22', 648043.76000000001, 'paid', 46);
INSERT INTO payment VALUES (47, '2001-06-05', 40448.440000000002, 'not paid', 47);
INSERT INTO payment VALUES (48, '2015-05-11', 81011.289999999994, 'paid', 48);
INSERT INTO payment VALUES (49, '2015-12-22', 18234.619999999999, 'paid', 49);
INSERT INTO payment VALUES (50, '2010-05-16', 58.060000000000002, 'paid', 50);


--
-- TOC entry 2794 (class 2606 OID 50701)
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (payment_id);


--
-- TOC entry 2795 (class 2606 OID 50702)
-- Name: payment payment_customer_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(customer_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:15:07

--
-- PostgreSQL database dump complete
--

