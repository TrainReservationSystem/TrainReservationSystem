--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:16:31

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 220 (class 1259 OID 50407)
-- Name: customer; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE customer (
    customer_id integer DEFAULT nextval('customer_id_seq'::regclass) NOT NULL,
    username character varying(20) NOT NULL
);


ALTER TABLE customer OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 50407)
-- Dependencies: 220
-- Data for Name: customer; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO customer VALUES (1, 'Siska32');
INSERT INTO customer VALUES (2, 'Cian');
INSERT INTO customer VALUES (3, 'Lu2');
INSERT INTO customer VALUES (4, 'Bertje128');
INSERT INTO customer VALUES (5, 'Ed744');
INSERT INTO customer VALUES (6, 'Joanne514');
INSERT INTO customer VALUES (7, 'Iris140');
INSERT INTO customer VALUES (8, 'Alva094');
INSERT INTO customer VALUES (9, 'Catherine7');
INSERT INTO customer VALUES (10, 'Alexander5');
INSERT INTO customer VALUES (11, 'Jaclyn');
INSERT INTO customer VALUES (12, 'Jim');
INSERT INTO customer VALUES (13, 'Lu460');
INSERT INTO customer VALUES (14, 'Peggy428');
INSERT INTO customer VALUES (15, 'Oscar');
INSERT INTO customer VALUES (16, 'Daniel');
INSERT INTO customer VALUES (17, 'Camille197');
INSERT INTO customer VALUES (18, 'Philip');
INSERT INTO customer VALUES (19, 'Anton49');
INSERT INTO customer VALUES (20, 'Liza667');
INSERT INTO customer VALUES (21, 'Cecilie393');
INSERT INTO customer VALUES (22, 'Richard30');
INSERT INTO customer VALUES (23, 'Dylan');
INSERT INTO customer VALUES (24, 'Bertje');
INSERT INTO customer VALUES (25, 'Nathan');
INSERT INTO customer VALUES (26, 'Ainhoa');
INSERT INTO customer VALUES (27, 'Claudia');
INSERT INTO customer VALUES (28, 'Klara25');
INSERT INTO customer VALUES (29, 'Callum3');
INSERT INTO customer VALUES (30, 'Lincoln');
INSERT INTO customer VALUES (31, 'Jessica');
INSERT INTO customer VALUES (32, 'Mary');
INSERT INTO customer VALUES (33, 'Will62');
INSERT INTO customer VALUES (34, 'Alfons563');
INSERT INTO customer VALUES (35, 'Mart7');
INSERT INTO customer VALUES (36, 'Scott');
INSERT INTO customer VALUES (37, 'Margarita');
INSERT INTO customer VALUES (38, 'Joop919');
INSERT INTO customer VALUES (39, 'Scott9');
INSERT INTO customer VALUES (40, 'Lukas353');
INSERT INTO customer VALUES (41, 'Jeanette89');
INSERT INTO customer VALUES (42, 'HeroZero');
INSERT INTO customer VALUES (43, 'Gillian8');
INSERT INTO customer VALUES (44, 'Vincent33');
INSERT INTO customer VALUES (45, 'Ellie');
INSERT INTO customer VALUES (46, 'Jolanda3');
INSERT INTO customer VALUES (47, 'Jordy4');
INSERT INTO customer VALUES (48, 'Lucy89');
INSERT INTO customer VALUES (49, 'Hannah');
INSERT INTO customer VALUES (50, 'Lauren8');
INSERT INTO customer VALUES (51, 'Vincent');


--
-- TOC entry 2794 (class 2606 OID 50411)
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (customer_id);


--
-- TOC entry 2795 (class 2606 OID 50412)
-- Name: customer customer_username_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_username_fkey FOREIGN KEY (username) REFERENCES user_(username) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:16:31

--
-- PostgreSQL database dump complete
--

