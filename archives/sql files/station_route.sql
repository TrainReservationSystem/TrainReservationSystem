--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:14:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 229 (class 1259 OID 50530)
-- Name: station_route; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE station_route (
    station_route_id integer NOT NULL,
    route_id integer NOT NULL,
    station_id integer NOT NULL,
    arrival_time time without time zone NOT NULL,
    departure_time time without time zone NOT NULL
);


ALTER TABLE station_route OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 50530)
-- Dependencies: 229
-- Data for Name: station_route; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO station_route VALUES (1, 20, 1, '02:16:00', '09:46:00');
INSERT INTO station_route VALUES (2, 19, 2, '02:37:00', '07:09:00');
INSERT INTO station_route VALUES (3, 18, 3, '07:21:00', '05:49:00');
INSERT INTO station_route VALUES (4, 17, 4, '06:43:00', '02:09:00');
INSERT INTO station_route VALUES (5, 16, 5, '02:57:00', '06:53:00');
INSERT INTO station_route VALUES (6, 15, 6, '07:15:00', '07:12:00');
INSERT INTO station_route VALUES (7, 14, 7, '04:21:00', '04:04:00');
INSERT INTO station_route VALUES (8, 13, 8, '10:32:00', '08:58:00');
INSERT INTO station_route VALUES (9, 12, 9, '06:57:00', '01:19:00');
INSERT INTO station_route VALUES (10, 11, 10, '03:43:00', '03:07:00');
INSERT INTO station_route VALUES (11, 10, 9, '07:21:00', '01:12:00');
INSERT INTO station_route VALUES (12, 9, 8, '06:23:00', '01:40:00');
INSERT INTO station_route VALUES (13, 8, 7, '00:21:00', '04:55:00');
INSERT INTO station_route VALUES (14, 7, 6, '03:26:00', '05:58:00');
INSERT INTO station_route VALUES (15, 6, 5, '06:43:00', '10:57:00');
INSERT INTO station_route VALUES (16, 5, 4, '05:23:00', '10:27:00');
INSERT INTO station_route VALUES (17, 4, 3, '00:11:00', '04:05:00');
INSERT INTO station_route VALUES (18, 3, 2, '10:09:00', '00:31:00');
INSERT INTO station_route VALUES (19, 2, 1, '00:28:00', '01:16:00');
INSERT INTO station_route VALUES (20, 1, 10, '03:33:00', '00:21:00');


--
-- TOC entry 2793 (class 2606 OID 50534)
-- Name: station_route station_route_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY station_route
    ADD CONSTRAINT station_route_pkey PRIMARY KEY (station_route_id);


--
-- TOC entry 2794 (class 2606 OID 50535)
-- Name: station_route station_route_route_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY station_route
    ADD CONSTRAINT station_route_route_id_fkey FOREIGN KEY (route_id) REFERENCES route(route_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2795 (class 2606 OID 50540)
-- Name: station_route station_route_station_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY station_route
    ADD CONSTRAINT station_route_station_id_fkey FOREIGN KEY (station_id) REFERENCES station(station_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:14:09

--
-- PostgreSQL database dump complete
--

