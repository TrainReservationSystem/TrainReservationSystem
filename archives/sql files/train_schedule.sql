--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:12:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 230 (class 1259 OID 50547)
-- Name: train_schedule; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE train_schedule (
    schedule_id integer NOT NULL,
    date date NOT NULL,
    departure_time time without time zone NOT NULL,
    train_id integer NOT NULL
);


ALTER TABLE train_schedule OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 50547)
-- Dependencies: 230
-- Data for Name: train_schedule; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO train_schedule VALUES (2, '2009-10-21', '04:16:00', 2);
INSERT INTO train_schedule VALUES (4, '2016-05-22', '04:25:00', 4);
INSERT INTO train_schedule VALUES (5, '2002-05-12', '10:58:00', 5);
INSERT INTO train_schedule VALUES (6, '2017-04-20', '06:20:00', 6);
INSERT INTO train_schedule VALUES (7, '2013-05-21', '07:25:00', 7);
INSERT INTO train_schedule VALUES (8, '2008-06-03', '04:36:00', 8);
INSERT INTO train_schedule VALUES (9, '2005-11-03', '01:34:00', 9);
INSERT INTO train_schedule VALUES (10, '2015-03-19', '09:14:00', 10);
INSERT INTO train_schedule VALUES (11, '2009-08-29', '07:54:00', 11);
INSERT INTO train_schedule VALUES (12, '2007-05-20', '05:02:00', 12);
INSERT INTO train_schedule VALUES (13, '2009-07-25', '10:25:00', 13);
INSERT INTO train_schedule VALUES (14, '2010-05-12', '10:03:00', 14);
INSERT INTO train_schedule VALUES (15, '2013-12-25', '01:56:00', 15);
INSERT INTO train_schedule VALUES (16, '2001-03-18', '00:53:00', 16);
INSERT INTO train_schedule VALUES (17, '2000-11-18', '03:09:00', 17);
INSERT INTO train_schedule VALUES (18, '2006-10-10', '05:33:00', 18);
INSERT INTO train_schedule VALUES (19, '2004-07-20', '01:54:00', 19);
INSERT INTO train_schedule VALUES (20, '2012-10-25', '09:54:00', 20);
INSERT INTO train_schedule VALUES (3, '2018-09-23', '10:31:00', 3);
INSERT INTO train_schedule VALUES (21, '2018-05-26', '02:58:00', 3);
INSERT INTO train_schedule VALUES (1, '2018-05-26', '10:31:00', 1);


--
-- TOC entry 2793 (class 2606 OID 50551)
-- Name: train_schedule train_schedule_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_schedule
    ADD CONSTRAINT train_schedule_pkey PRIMARY KEY (schedule_id);


--
-- TOC entry 2794 (class 2606 OID 50552)
-- Name: train_schedule train_schedule_train_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train_schedule
    ADD CONSTRAINT train_schedule_train_id_fkey FOREIGN KEY (train_id) REFERENCES train(train_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:12:20

--
-- PostgreSQL database dump complete
--

