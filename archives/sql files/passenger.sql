--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:15:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 231 (class 1259 OID 50557)
-- Name: passenger; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE passenger (
    id_card character varying(20) NOT NULL,
    name character varying(50) NOT NULL,
    phone_no character varying(20) NOT NULL,
    dob date NOT NULL,
    seat_id integer NOT NULL
);


ALTER TABLE passenger OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 50557)
-- Dependencies: 231
-- Data for Name: passenger; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO passenger VALUES ('kI0iqCHW6zqnbiW3mm', 'Pb', '325-174-6041', '2006-10-20', 1);
INSERT INTO passenger VALUES ('uHx8Na2im62ox', 'olbNydGncarQY', '492-541-2754', '2007-04-16', 2);
INSERT INTO passenger VALUES ('xFQGNEmj8x', 'H4OyKwjvoHet8qvApflfdOlkggGG', '563-352-5112', '2016-07-20', 3);
INSERT INTO passenger VALUES ('D6h44K77iERheg4TI', 'kfgtyp2u5qxJdDNrER6lAL3ASmzOY2cnqMTqUucAj2lv', '1-091-2866', '2004-09-30', 4);
INSERT INTO passenger VALUES ('AqU0GcTf4SVD', 'Kmdoe', '1-545-151-6940', '2015-05-17', 5);
INSERT INTO passenger VALUES ('Pz740', 'JmFZ', '1-671-291-1778', '2009-10-01', 6);
INSERT INTO passenger VALUES ('ez5ZuDUjVp4', 'dBS7pWczpF6hjituqGmmiDlkhLJ2Xvwr', '840-1046', '2012-12-24', 7);
INSERT INTO passenger VALUES ('bBrzxxy5eK3N4p20', '4wWGCKTu5PpPHBJ7MhM6U', '304-458-0133', '2003-01-26', 8);
INSERT INTO passenger VALUES ('C8qp8jtTbwy6lX4eFzk', '5RsTIP10lvraIIk2fFD36voe', '563-4205', '2015-07-04', 9);
INSERT INTO passenger VALUES ('vWJ5EZp58mIbW8h4', 'zUzF2F', '918-571-6859', '2008-07-16', 10);
INSERT INTO passenger VALUES ('R2mbef3xoyEBwhRh0Dbw', 'mHId1aoLYCsRccGg8XqHjzPtvXFGPJm8mEmKUjYG', '1-323-881-8977', '2003-01-25', 11);
INSERT INTO passenger VALUES ('ldKjhHO0g', 'IH8x2Csv8I0gagBhpvtLTVLi83w4urnKjm1TXN4l4g0xANvbG', '431-9210', '2017-09-03', 12);
INSERT INTO passenger VALUES ('lvFWd4jgXK', '7f043SV4NQ3U', '1-423-0170', '2000-02-13', 13);
INSERT INTO passenger VALUES ('kwPVXr', 'P8K1WK2zycwQiNrk3ZXxmZWg', '1-782-9705', '2005-01-12', 14);
INSERT INTO passenger VALUES ('V5', 'BP8eie58SGxgNk3U6Ge3uw1cxgSsUAV83Q3JCuLcJq2xkK', '1-313-2316', '2011-05-22', 15);
INSERT INTO passenger VALUES ('ZHHAshjVN', 'BQA3Dg3rEf2a', '632-625-0207', '2008-07-22', 16);
INSERT INTO passenger VALUES ('zij', '4mgZa0EvvFvxwhlxiVuwbzyfo0QgI2PcWzgcsod0HI56gpLtTc', '1-151-492-0349', '2010-07-08', 17);
INSERT INTO passenger VALUES ('U', '4UzNNaEs6xCerIHl4x1QvRvuMbTgxcJPjBhN1', '1-992-5749', '2003-11-03', 18);
INSERT INTO passenger VALUES ('PMrovuaTHLvyRl3f', 'IsCqSoN3qb5MQkPXzrTuBByJHsqjoLG3H', '386-096-4977', '2011-02-24', 19);
INSERT INTO passenger VALUES ('weBYrHO4OKPySLbXhlN', 'nOWEPjTgw8', '1-106-3774', '2011-04-03', 20);
INSERT INTO passenger VALUES ('eID3TC', 'tGQhPlVx2mGjjzkT1hUqE3AQHLKy16Lq8NgT0W2KdwwEMD', '1-117-116-2326', '2004-08-21', 21);
INSERT INTO passenger VALUES ('JOdPMoK1NPCERyiQht', '8a363AsUxOuPW2M3bU1RqV4sBQ0Z4SmlXns', '1-116-1925', '2015-09-25', 22);
INSERT INTO passenger VALUES ('Kn', 'WrIA6IDB', '1-628-060-0673', '2005-12-13', 23);
INSERT INTO passenger VALUES ('BQlv3u7AIVW', 'FTbMXYlRkuO4', '133-561-8313', '2001-11-20', 24);
INSERT INTO passenger VALUES ('AfM2ujAouvyVFR', 'OqgVNC1sGFlqs4SUj0UnXiVXY5aRyaCp1F5l6yFi', '1-347-8314', '2008-04-05', 25);
INSERT INTO passenger VALUES ('kSTHzol2', 'WzHdWLFZjWL', '1-818-978-9323', '2017-02-19', 26);
INSERT INTO passenger VALUES ('QmgCPt3djqFuFCIF', 'W4NkBxLDDQ0lB', '228-182-0347', '2007-08-26', 27);
INSERT INTO passenger VALUES ('aZGbcK4nUk1y', 'k07SeyU8SPX2oHG', '1-001-7830', '2013-07-27', 28);
INSERT INTO passenger VALUES ('GslmvxyddV74', 'rmeLyYeaS5Oef7THoBTAXUG0l5Qas7Cqtwg8Y0KkJ0L84SQI', '1-117-427-9021', '2002-04-17', 29);
INSERT INTO passenger VALUES ('uDk73', '50Cp5n3pNq0PjnYafIOmIwDwbMRFJ', '1-069-119-9676', '2001-05-08', 30);
INSERT INTO passenger VALUES ('Fj7BOmB', 'KVpkIvpW7Q4jKKfWYWIsFyMSYLomTj1Gfu3X', '1-810-4132', '2000-07-12', 31);
INSERT INTO passenger VALUES ('P20ji6tIZewph2', 'MvUlWwxZ3qw', '206-840-6081', '2011-03-02', 32);
INSERT INTO passenger VALUES ('jqmhVpNU', '1CDceQe144', '1-273-8259', '2001-02-12', 33);
INSERT INTO passenger VALUES ('fOr7HKg2ZtPNMPo5', 'chaobWvGou', '154-8557', '2009-06-05', 35);
INSERT INTO passenger VALUES ('CzGPLAfwo2', 'wniMOe4I', '1-996-5100', '2014-03-19', 36);
INSERT INTO passenger VALUES ('xugfEkR0nx8LEy3M', 'jR2sjFs2x5KwHeSLkSypkQ', '1-239-653-4471', '2004-10-02', 37);
INSERT INTO passenger VALUES ('vqGuOPx66GLfelxyRy', 'rXdHpevZbWqMvblv0IaAdMIrzmyeTvRui22', '403-941-7588', '2011-08-23', 38);
INSERT INTO passenger VALUES ('sYx2uUbr20VA8es', 'W2ZUyVIZMJm', '335-564-4340', '2014-09-06', 39);
INSERT INTO passenger VALUES ('a2BUw4v', 'kyJiipp0LNe34JddLqUqhqSbhMgUh0H6tjIorkiTRyyB3', '1-293-070-8648', '2003-10-19', 40);
INSERT INTO passenger VALUES ('lJ2jbqobaJc2K6cFLQb', 'IT35mH7d2BMNpEXlRrhK7UfFbD4jVevNc2HRdG65IyeLc5QLp', '1-649-914-0610', '2013-08-01', 41);
INSERT INTO passenger VALUES ('bvER4uYgcufoJs', '0sKnflmP7gy6UlzF3jhqfIk4mNjucWkz5LQdnu5mGTJeQS4WY4', '1-224-777-7173', '2004-10-31', 42);
INSERT INTO passenger VALUES ('tgqV5upFeW', 'DaVfc4Uo5UxzPxk', '614-2166', '2014-06-14', 43);
INSERT INTO passenger VALUES ('CS7my7LPwTLsWQw8MD', 'jS24YPvMDbWkILahc0k05SnXdX8', '1-979-742-0348', '2011-02-07', 44);
INSERT INTO passenger VALUES ('SUg0EK1aeBiVugb6lJ', '0MuTishL5Hmu8I0DS3c', '1-967-190-5013', '2000-11-22', 46);
INSERT INTO passenger VALUES ('vZireJe', 'EHHfynPlgo8d0ysE76CaPk0h', '552-9633', '2013-10-17', 47);
INSERT INTO passenger VALUES ('V', 'dy21zawP5iN0NycyxozMowRD13jWe', '569-925-7638', '2006-10-13', 48);
INSERT INTO passenger VALUES ('cMgT', 'NLPSpDPYTINx4U1B', '1-804-5132', '2016-11-19', 49);
INSERT INTO passenger VALUES ('n72ttlxPPPSgL3Og', 'iaLNgBqUtquKidk', '976-3962', '2011-09-07', 50);
INSERT INTO passenger VALUES ('aiwfdnKNAWF', 'eqtqv0ZHCoKwBSgRrwX', '1-703-484-0444', '2011-10-26', 55);
INSERT INTO passenger VALUES ('ABCD', 'IO0Ke3dcxIcoYbei22KpKYIWeR8GMbUxgDVfhgIyUV', '884-997-0808', '2017-03-07', 45);
INSERT INTO passenger VALUES ('1', 'a', '2', '2018-05-01', 51);
INSERT INTO passenger VALUES ('3', 'b', '4', '2018-05-02', 52);
INSERT INTO passenger VALUES ('1234', 'Haha', '0987654321', '2018-05-31', 34);
INSERT INTO passenger VALUES ('0987', 'Haha', '1234567890', '2018-05-12', 53);


--
-- TOC entry 2793 (class 2606 OID 50561)
-- Name: passenger passenger_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY passenger
    ADD CONSTRAINT passenger_pkey PRIMARY KEY (id_card);


--
-- TOC entry 2794 (class 2606 OID 50562)
-- Name: passenger passenger_seat_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY passenger
    ADD CONSTRAINT passenger_seat_id_fkey FOREIGN KEY (seat_id) REFERENCES seat(seat_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:15:25

--
-- PostgreSQL database dump complete
--

