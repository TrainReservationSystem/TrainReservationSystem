--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:15:37

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 235 (class 1259 OID 50602)
-- Name: food_payment; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE food_payment (
    food_payment_id integer NOT NULL,
    payment_date date NOT NULL,
    total_payment double precision NOT NULL,
    status character varying(10) NOT NULL,
    customer_id integer NOT NULL,
    CONSTRAINT chk_status CHECK ((((status)::text = 'paid'::text) OR ((status)::text = 'not paid'::text)))
);


ALTER TABLE food_payment OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 50602)
-- Dependencies: 235
-- Data for Name: food_payment; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO food_payment VALUES (1, '2009-05-22', 1333.73, 'paid', 1);
INSERT INTO food_payment VALUES (2, '2002-12-29', 483284.09999999998, 'not paid', 2);
INSERT INTO food_payment VALUES (3, '2005-10-12', 78748.139999999999, 'paid', 3);
INSERT INTO food_payment VALUES (4, '2004-01-12', 6.4100000000000001, 'not paid', 4);
INSERT INTO food_payment VALUES (5, '2006-07-12', 64.870000000000005, 'paid', 5);
INSERT INTO food_payment VALUES (6, '2008-11-20', 845726.43999999994, 'paid', 6);
INSERT INTO food_payment VALUES (7, '2007-10-24', 38330.440000000002, 'paid', 7);
INSERT INTO food_payment VALUES (8, '2007-11-29', 705.83000000000004, 'not paid', 26);
INSERT INTO food_payment VALUES (9, '2004-10-08', 9591.2999999999993, 'paid', 8);
INSERT INTO food_payment VALUES (10, '2010-05-19', 2.2599999999999998, 'paid', 9);
INSERT INTO food_payment VALUES (11, '2015-08-30', 22778.52, 'not paid', 10);
INSERT INTO food_payment VALUES (12, '2016-12-28', 135.84999999999999, 'paid', 11);
INSERT INTO food_payment VALUES (13, '2016-07-16', 550196.91000000003, 'paid', 12);
INSERT INTO food_payment VALUES (14, '2017-05-03', 617163.62, 'paid', 13);
INSERT INTO food_payment VALUES (15, '2014-03-21', 64953.669999999998, 'paid', 14);
INSERT INTO food_payment VALUES (16, '2008-10-07', 687.27999999999997, 'paid', 15);
INSERT INTO food_payment VALUES (17, '2001-05-28', 9.75, 'paid', 16);
INSERT INTO food_payment VALUES (18, '2008-08-10', 17.91, 'paid', 17);
INSERT INTO food_payment VALUES (19, '2012-02-06', 57420.760000000002, 'not paid', 18);
INSERT INTO food_payment VALUES (20, '2007-09-29', 42.100000000000001, 'paid', 19);
INSERT INTO food_payment VALUES (21, '2011-01-16', 9418.8199999999997, 'not paid', 20);
INSERT INTO food_payment VALUES (22, '2006-08-27', 11866.16, 'paid', 21);
INSERT INTO food_payment VALUES (23, '2005-11-16', 25761.619999999999, 'paid', 22);
INSERT INTO food_payment VALUES (24, '2009-07-28', 3.2999999999999998, 'paid', 23);
INSERT INTO food_payment VALUES (25, '2002-01-09', 871216.81999999995, 'paid', 24);
INSERT INTO food_payment VALUES (26, '2011-09-30', 277095.22999999998, 'paid', 25);
INSERT INTO food_payment VALUES (27, '2004-10-31', 609892.44999999995, 'paid', 27);
INSERT INTO food_payment VALUES (28, '2002-01-31', 9.6099999999999994, 'paid', 28);
INSERT INTO food_payment VALUES (29, '2006-03-08', 16144.370000000001, 'paid', 29);
INSERT INTO food_payment VALUES (30, '2004-07-11', 33204.629999999997, 'paid', 30);
INSERT INTO food_payment VALUES (31, '2018-04-24', 5589.4899999999998, 'paid', 31);
INSERT INTO food_payment VALUES (32, '2017-07-04', 56.219999999999999, 'paid', 32);
INSERT INTO food_payment VALUES (33, '2008-06-30', 7816.6400000000003, 'paid', 33);
INSERT INTO food_payment VALUES (34, '2003-09-22', 8.6699999999999999, 'paid', 34);
INSERT INTO food_payment VALUES (35, '2006-06-22', 20779.52, 'paid', 35);
INSERT INTO food_payment VALUES (36, '2008-06-19', 50.920000000000002, 'not paid', 36);
INSERT INTO food_payment VALUES (37, '2004-04-28', 129.63999999999999, 'paid', 37);
INSERT INTO food_payment VALUES (38, '2010-05-05', 87899.740000000005, 'paid', 38);
INSERT INTO food_payment VALUES (39, '2000-04-20', 169.91, 'paid', 39);
INSERT INTO food_payment VALUES (40, '2012-07-31', 179975.16, 'paid', 40);
INSERT INTO food_payment VALUES (41, '2008-11-18', 85851.919999999998, 'paid', 41);
INSERT INTO food_payment VALUES (42, '2007-03-30', 19.620000000000001, 'paid', 42);
INSERT INTO food_payment VALUES (43, '2001-05-09', 4188.3599999999997, 'paid', 43);
INSERT INTO food_payment VALUES (44, '2010-04-11', 1.78, 'paid', 44);
INSERT INTO food_payment VALUES (45, '2009-08-01', 10.529999999999999, 'paid', 45);
INSERT INTO food_payment VALUES (46, '2002-01-06', 2504.75, 'paid', 46);
INSERT INTO food_payment VALUES (47, '2003-02-02', 85.400000000000006, 'not paid', 47);
INSERT INTO food_payment VALUES (48, '2015-04-02', 558.13, 'paid', 48);
INSERT INTO food_payment VALUES (49, '2017-11-04', 70.459999999999994, 'not paid', 49);
INSERT INTO food_payment VALUES (50, '2017-04-23', 1.73, 'paid', 50);


--
-- TOC entry 2794 (class 2606 OID 50607)
-- Name: food_payment food_payment_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY food_payment
    ADD CONSTRAINT food_payment_pkey PRIMARY KEY (food_payment_id);


--
-- TOC entry 2795 (class 2606 OID 50608)
-- Name: food_payment food_payment_customer_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY food_payment
    ADD CONSTRAINT food_payment_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(customer_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:15:37

--
-- PostgreSQL database dump complete
--

