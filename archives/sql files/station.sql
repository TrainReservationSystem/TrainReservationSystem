--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:14:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 228 (class 1259 OID 50520)
-- Name: station; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE station (
    station_id integer NOT NULL,
    station_name character varying(20) NOT NULL,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    street character varying(20) NOT NULL,
    city character varying(20) NOT NULL,
    zipcode character varying(10) NOT NULL,
    available_track integer NOT NULL,
    updated_by integer NOT NULL
);


ALTER TABLE station OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 50520)
-- Dependencies: 228
-- Data for Name: station; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO station VALUES (1, 'One', 888562.80000000005, 5106.6700000000001, 'P', 'sj3ZVpMyB2k5Z22', '28506', 9, 1);
INSERT INTO station VALUES (4, 'Four', 771378.06000000006, 98.359999999999999, 'QJJqaueMLc2buGP0Jz', 'oW1h2IYHDkGnw2Dsm', '99138', 7, 4);
INSERT INTO station VALUES (3, 'Three', 7.25, 7402.9099999999999, 'jiGnQQDpowQxJ7bjQHWq', 'imy2LKVmbYf6', '06144', 4, 3);
INSERT INTO station VALUES (6, 'Six', 21.489999999999998, 5.3499999999999996, 'XCYgMBkQS', 'r', '13941', 7, 5);
INSERT INTO station VALUES (5, 'Five', 1115.1600000000001, 620512.58999999997, 'YSZgpVnuv', '5PD0y1Cqj', '81437', 4, 5);
INSERT INTO station VALUES (8, 'Eight', 83203.350000000006, 986.94000000000005, 'gDMG7fcmvUKFcRfQ8', 'Yp7ljF324GHs2EEe5nso', '54495', 5, 3);
INSERT INTO station VALUES (7, 'Seven', 176103.79000000001, 32.350000000000001, '6eBNyU55a', 'LYsghy', '59595', 4, 4);
INSERT INTO station VALUES (10, 'Ten', 1896.05, 42388.370000000003, 'NxeLU6DBagq2AudgyE', '2RQGQHxckl', '48096', 6, 1);
INSERT INTO station VALUES (9, 'Nine', 7627.1599999999999, 8112.5799999999999, 'IHcPwYcdcIEcf', 'Qx', '90768', 3, 2);
INSERT INTO station VALUES (2, 'Two', 266963.84999999998, 266963.84999999998, 'dj3NXYkf', 'sifQL8ipy', '87071', 7, 2);


--
-- TOC entry 2793 (class 2606 OID 50524)
-- Name: station station_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY station
    ADD CONSTRAINT station_pkey PRIMARY KEY (station_id);


--
-- TOC entry 2794 (class 2606 OID 50525)
-- Name: station station_updated_by_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY station
    ADD CONSTRAINT station_updated_by_fkey FOREIGN KEY (updated_by) REFERENCES system_admin(admin_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:14:19

--
-- PostgreSQL database dump complete
--

