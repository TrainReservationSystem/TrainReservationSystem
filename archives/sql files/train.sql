--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:13:35

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 225 (class 1259 OID 50452)
-- Name: train; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE train (
    train_id integer NOT NULL,
    train_name character varying(20) NOT NULL,
    class character varying(20) NOT NULL,
    capacity integer NOT NULL,
    route_id integer NOT NULL,
    coordinate_by integer NOT NULL,
    CONSTRAINT chk_class CHECK ((((class)::text = 'executive'::text) OR ((class)::text = 'business'::text) OR ((class)::text = 'economy'::text)))
);


ALTER TABLE train OWNER TO postgres;

--
-- TOC entry 2918 (class 0 OID 50452)
-- Dependencies: 225
-- Data for Name: train; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO train VALUES (27, 'XiutUPPj', 'economy', 100, 7, 4);
INSERT INTO train VALUES (21, 'nsV', 'economy', 100, 1, 10);
INSERT INTO train VALUES (23, 'VuARTocKvzC', 'economy', 100, 3, 8);
INSERT INTO train VALUES (24, 'DpnlGzQ23k', 'economy', 100, 4, 7);
INSERT INTO train VALUES (29, 'kTKCVU7zR4udll', 'economy', 100, 9, 2);
INSERT INTO train VALUES (3, 'jTrzciO2Rt', 'executive', 100, 3, 28);
INSERT INTO train VALUES (5, 'wp', 'executive', 100, 5, 26);
INSERT INTO train VALUES (7, '2', 'executive', 100, 7, 24);
INSERT INTO train VALUES (10, 'X4TZL0kuoNtp', 'executive', 100, 10, 21);
INSERT INTO train VALUES (9, 'eUmZdBVcZ1a', 'executive', 100, 9, 22);
INSERT INTO train VALUES (14, 'J7sWUkT2e0J7', 'business', 100, 14, 17);
INSERT INTO train VALUES (13, 'vnYO', 'business', 100, 13, 18);
INSERT INTO train VALUES (17, '8UQzcHJbJ1', 'business', 100, 17, 14);
INSERT INTO train VALUES (19, 'Kh7QJNQbNzhZ', 'business', 100, 19, 12);
INSERT INTO train VALUES (12, 'MkOGXWU31cQ', 'business', 100, 12, 19);
INSERT INTO train VALUES (11, '4AtDVNQjzItYn', 'business', 100, 11, 20);
INSERT INTO train VALUES (28, 'kxKjEiE3GOJ', 'economy', 100, 8, 3);
INSERT INTO train VALUES (16, 'kWrWSCbJWjbT', 'business', 100, 16, 15);
INSERT INTO train VALUES (15, 'oqe70sahU4u', 'business', 100, 15, 16);
INSERT INTO train VALUES (18, '2wlN', 'business', 100, 18, 13);
INSERT INTO train VALUES (26, 'Z5SyxRca7yqyH8', 'economy', 100, 6, 5);
INSERT INTO train VALUES (20, 'vCFlWm0Ql0', 'business', 100, 20, 11);
INSERT INTO train VALUES (2, 'j6t4BByCWcC', 'executive', 100, 2, 29);
INSERT INTO train VALUES (4, '4QLLG2OrQ', 'executive', 100, 4, 27);
INSERT INTO train VALUES (6, 'HXrLlenayl', 'executive', 100, 6, 25);
INSERT INTO train VALUES (8, 'OPK2D', 'executive', 100, 8, 23);
INSERT INTO train VALUES (25, '7', 'economy', 100, 5, 6);
INSERT INTO train VALUES (22, 'A', 'economy', 100, 2, 9);
INSERT INTO train VALUES (30, 'nPopidFzhkt7', 'economy', 100, 10, 1);
INSERT INTO train VALUES (1, 'okF0nChxZblr', 'executive', 100, 3, 30);


--
-- TOC entry 2794 (class 2606 OID 50456)
-- Name: train train_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train
    ADD CONSTRAINT train_pkey PRIMARY KEY (train_id);


--
-- TOC entry 2796 (class 2606 OID 50462)
-- Name: train train_coordinate_by_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train
    ADD CONSTRAINT train_coordinate_by_fkey FOREIGN KEY (coordinate_by) REFERENCES train_conductor(conductor_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2795 (class 2606 OID 50457)
-- Name: train train_route_id_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY train
    ADD CONSTRAINT train_route_id_fkey FOREIGN KEY (route_id) REFERENCES route(route_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:13:35

--
-- PostgreSQL database dump complete
--

