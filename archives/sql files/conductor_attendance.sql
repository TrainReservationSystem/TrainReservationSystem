--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:16:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 223 (class 1259 OID 50437)
-- Name: conductor_attendance; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE conductor_attendance (
    attendance_id integer DEFAULT nextval('train_attendance_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    check_in_time time without time zone NOT NULL,
    check_out_time time without time zone NOT NULL,
    id_conductor integer NOT NULL
);


ALTER TABLE conductor_attendance OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 50437)
-- Dependencies: 223
-- Data for Name: conductor_attendance; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO conductor_attendance VALUES (1, '2013-02-26', '06:25:00', '09:53:00', 1);
INSERT INTO conductor_attendance VALUES (2, '2018-01-24', '01:31:00', '02:10:00', 2);
INSERT INTO conductor_attendance VALUES (3, '2003-11-29', '04:40:00', '02:49:00', 3);
INSERT INTO conductor_attendance VALUES (4, '2017-09-05', '10:50:00', '05:27:00', 4);
INSERT INTO conductor_attendance VALUES (5, '2000-06-08', '09:27:00', '02:45:00', 5);
INSERT INTO conductor_attendance VALUES (6, '2006-01-26', '07:51:00', '08:47:00', 6);
INSERT INTO conductor_attendance VALUES (7, '2016-11-27', '07:48:00', '01:05:00', 7);
INSERT INTO conductor_attendance VALUES (8, '2015-11-14', '01:49:00', '04:54:00', 8);
INSERT INTO conductor_attendance VALUES (9, '2003-08-22', '02:43:00', '10:25:00', 9);
INSERT INTO conductor_attendance VALUES (10, '2015-11-12', '06:56:00', '04:47:00', 10);
INSERT INTO conductor_attendance VALUES (11, '2012-01-12', '03:05:00', '02:32:00', 11);
INSERT INTO conductor_attendance VALUES (12, '2002-08-13', '01:15:00', '03:34:00', 12);
INSERT INTO conductor_attendance VALUES (13, '2011-04-17', '05:37:00', '02:27:00', 13);
INSERT INTO conductor_attendance VALUES (14, '2000-06-18', '02:42:00', '06:35:00', 14);
INSERT INTO conductor_attendance VALUES (15, '2016-09-29', '09:58:00', '08:22:00', 15);
INSERT INTO conductor_attendance VALUES (16, '2006-07-18', '09:09:00', '01:24:00', 16);
INSERT INTO conductor_attendance VALUES (17, '2016-07-02', '10:42:00', '04:41:00', 17);
INSERT INTO conductor_attendance VALUES (18, '2009-02-09', '09:19:00', '04:47:00', 18);
INSERT INTO conductor_attendance VALUES (19, '2008-02-17', '00:14:00', '02:03:00', 19);
INSERT INTO conductor_attendance VALUES (20, '2005-11-13', '03:48:00', '08:42:00', 20);
INSERT INTO conductor_attendance VALUES (21, '2016-12-19', '09:52:00', '00:14:00', 21);
INSERT INTO conductor_attendance VALUES (22, '2003-12-24', '02:10:00', '10:19:00', 22);
INSERT INTO conductor_attendance VALUES (23, '2004-06-29', '08:49:00', '03:16:00', 23);
INSERT INTO conductor_attendance VALUES (24, '2001-09-20', '09:46:00', '06:00:00', 24);
INSERT INTO conductor_attendance VALUES (25, '2014-12-15', '08:12:00', '00:22:00', 25);
INSERT INTO conductor_attendance VALUES (26, '2006-10-27', '03:55:00', '07:19:00', 26);
INSERT INTO conductor_attendance VALUES (27, '2010-05-26', '04:57:00', '00:00:00', 27);
INSERT INTO conductor_attendance VALUES (28, '2018-02-18', '00:49:00', '01:02:00', 28);
INSERT INTO conductor_attendance VALUES (29, '2017-04-25', '07:02:00', '06:43:00', 29);
INSERT INTO conductor_attendance VALUES (30, '2007-04-21', '02:31:00', '03:09:00', 30);
INSERT INTO conductor_attendance VALUES (31, '2016-08-31', '04:09:00', '07:25:00', 1);
INSERT INTO conductor_attendance VALUES (32, '2010-06-01', '06:00:00', '02:50:00', 2);
INSERT INTO conductor_attendance VALUES (33, '2017-03-03', '07:57:00', '10:07:00', 3);
INSERT INTO conductor_attendance VALUES (34, '2018-01-06', '04:06:00', '07:29:00', 4);
INSERT INTO conductor_attendance VALUES (35, '2000-05-12', '04:04:00', '07:02:00', 5);
INSERT INTO conductor_attendance VALUES (36, '2011-10-10', '06:35:00', '10:41:00', 6);
INSERT INTO conductor_attendance VALUES (37, '2012-03-14', '00:52:00', '08:36:00', 7);
INSERT INTO conductor_attendance VALUES (38, '2001-02-19', '06:05:00', '00:13:00', 8);
INSERT INTO conductor_attendance VALUES (39, '2016-06-15', '06:43:00', '08:30:00', 9);
INSERT INTO conductor_attendance VALUES (40, '2005-01-01', '05:08:00', '06:05:00', 10);
INSERT INTO conductor_attendance VALUES (41, '2004-09-07', '06:50:00', '10:32:00', 11);
INSERT INTO conductor_attendance VALUES (42, '2015-01-24', '08:08:00', '09:29:00', 12);
INSERT INTO conductor_attendance VALUES (43, '2014-07-13', '01:21:00', '00:47:00', 13);
INSERT INTO conductor_attendance VALUES (44, '2014-02-20', '07:24:00', '07:48:00', 14);
INSERT INTO conductor_attendance VALUES (45, '2001-06-22', '10:14:00', '06:30:00', 15);
INSERT INTO conductor_attendance VALUES (46, '2012-10-27', '10:09:00', '07:40:00', 16);
INSERT INTO conductor_attendance VALUES (47, '2003-09-25', '04:24:00', '06:08:00', 17);
INSERT INTO conductor_attendance VALUES (48, '2012-10-27', '10:28:00', '00:52:00', 18);
INSERT INTO conductor_attendance VALUES (49, '2014-01-02', '04:37:00', '10:20:00', 19);
INSERT INTO conductor_attendance VALUES (50, '2006-12-27', '07:19:00', '05:26:00', 20);


--
-- TOC entry 2794 (class 2606 OID 50546)
-- Name: conductor_attendance conductor_attendance_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY conductor_attendance
    ADD CONSTRAINT conductor_attendance_pkey PRIMARY KEY (attendance_id, id_conductor);


--
-- TOC entry 2795 (class 2606 OID 50442)
-- Name: conductor_attendance conductor_attendance_id_conductor_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY conductor_attendance
    ADD CONSTRAINT conductor_attendance_id_conductor_fkey FOREIGN KEY (id_conductor) REFERENCES train_conductor(conductor_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:16:45

--
-- PostgreSQL database dump complete
--

