--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-06-02 16:13:50

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = train_reservation, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 221 (class 1259 OID 50417)
-- Name: system_admin; Type: TABLE; Schema: train_reservation; Owner: postgres
--

CREATE TABLE system_admin (
    admin_id integer NOT NULL,
    username character varying(20) NOT NULL
);


ALTER TABLE system_admin OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 50417)
-- Dependencies: 221
-- Data for Name: system_admin; Type: TABLE DATA; Schema: train_reservation; Owner: postgres
--

INSERT INTO system_admin VALUES (1, 'Ted6');
INSERT INTO system_admin VALUES (2, 'Luis');
INSERT INTO system_admin VALUES (3, 'Edward715');
INSERT INTO system_admin VALUES (4, 'Lara');
INSERT INTO system_admin VALUES (5, 'Sigrid308');


--
-- TOC entry 2793 (class 2606 OID 50421)
-- Name: system_admin system_admin_pkey; Type: CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY system_admin
    ADD CONSTRAINT system_admin_pkey PRIMARY KEY (admin_id);


--
-- TOC entry 2794 (class 2606 OID 50422)
-- Name: system_admin system_admin_username_fkey; Type: FK CONSTRAINT; Schema: train_reservation; Owner: postgres
--

ALTER TABLE ONLY system_admin
    ADD CONSTRAINT system_admin_username_fkey FOREIGN KEY (username) REFERENCES user_(username) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2018-06-02 16:13:51

--
-- PostgreSQL database dump complete
--

