import os
import string
import datetime
import psycopg2
import psycopg2.extras
from flask import Flask, render_template, session, request, jsonify

# # connect to local database
# # change this variables according your local database
# dbname = 'postgres'  # database name
# username = 'postgres'  # username
# password = 'admin'  # password

# conn_string = "host='localhost' dbname=%s user=%s password=%s " % (dbname, username, password)
# conn = psycopg2.connect(conn_string)
# conn.autocommit = True

# connect to heroku database
# change this variables according your heroku database
dbname = 'd9prav1lemv598'  # database name
username = 'xgilixdvddomua'  # username
password = 'aa847263a70393a11e50878dd234854c8a396d57b1c0dbd0c32ff651f6a73be6'  # password

conn_string = "host='ec2-54-204-46-236.compute-1.amazonaws.com' dbname=%s user=%s password=%s " % (dbname, username, password)
conn = psycopg2.connect(conn_string)
conn.autocommit = True

cur = conn.cursor()
dict_cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

now = datetime.datetime.now()

try:
    cur.execute('select * from train_reservation.system_admin')
    testObj = cur.fetchone()
except psycopg2.OperationalError:
    pass

app = Flask(__name__)
app.secret_key = os.urandom(12)

########################################## Made by Vincent ##########################################
@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('cover.html')
    else:
        return render_template('landing_page.html')

############################################# Made by Bayu ###############################################
@app.route('/signup')
def signupPage():
    return render_template('sign_up.html')

@app.route('/registration', methods=['POST'])
def registration():
    try:
        reqUsername = request.form["username"]
        reqEmail = request.form["email"]
        reqPassword = request.form["password"]
        reqPhoneNo = request.form["phone"]
        reqAddressNo = request.form["address"]
        reqStreet = request.form["street"]
        reqVillage = request.form["village"]
        reqSubDistrict = request.form["sub-district"]
        reqZip = request.form["zipcode"]

        cur.execute("SELECT * from train_reservation.user_ where username = '%s'" % (reqUsername))
        userObj = cur.fetchone()

        if len(reqUsername) > 20 or len(reqEmail) > 20 or len(reqPassword) > 20 or len(reqPhoneNo) > 20 or len(reqAddressNo) > 20 or len(reqVillage) > 20 or len(reqSubDistrict) > 20 or len(reqZip) > 20:
            return render_template("sign_up.html", charTooLong = True)
        if userObj is not None:
            return render_template("sign_up.html", userExist = True)
        if reqPassword.isalnum() and len(reqPassword) >= 8:
            # print ("before insertion")
            cur.execute('INSERT INTO train_reservation.user_ (username, email, password, phone_no, address_no, street, village, sub_district, zipcode) ' +
                        'VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)', (reqUsername, reqEmail, reqPassword, reqPhoneNo, reqAddressNo, reqStreet,
                                                                reqVillage, reqSubDistrict, reqZip))
            # print ("after insertion")
            dict_cur.execute("SELECT setval('train_reservation.customer_id_seq', (SELECT max(train_reservation.customer.customer_id) FROM train_reservation.customer))")
            cur.execute('INSERT INTO train_reservation.customer (customer_id, username) '
                       +'VALUES (default, %s)', (reqUsername,))
            return render_template('login.html', newUser = True)
        else:
            return render_template("sign_up.html", error = True)
                
    
    except Exception as e:
        print(str(e))
        return "System Error"

########################################## Made by Vincent ##########################################
@app.route('/login', defaults={'wrongPassword': False, 'notExist': False})
def loginPage(wrongPassword, notExist):
    if not session.get('logged_in'):
        return render_template('login.html', wrongPassword=wrongPassword, notExist=notExist)
    else:
        return render_template('landing_page.html')
    

@app.route('/login', methods=['POST'])
def login():
    try:
        reqUsername = request.form["username"]
        # print(reqUsername)
        reqPassword = request.form["password"]
        # print(reqPassword)

        cur.execute("SELECT * from train_reservation.user_ where username = '%s'" % (reqUsername))
        userObj = cur.fetchone()

        if userObj:
            correctPassword = userObj[2]
            requestEmail = userObj[1]
            # added variables
            reqPhoneNo = userObj[3]
            reqAdressNo = userObj[4]
            reqStreet = userObj[5]
            reqVillage = userObj[6]
            reqSubDistrict = userObj[7]
            reqzipcode = userObj[8]

            # print(requestName + " " + requestEmail + " " + requestPassword + " " + correctPassword)
            if reqPassword == correctPassword:
                session['email'] = requestEmail
                session['name'] = reqUsername
                userRole = getUserRole(reqUsername)
                print(userRole)
                session['role'] = userRole
                session['logged_in'] = True

                ############ Added by Bayu ############
                # added session variables
                session['phone_no'] = reqPhoneNo
                session['address_no'] = reqAdressNo
                session['street'] = reqStreet
                session['village'] = reqVillage
                session['sub_district'] = reqSubDistrict
                session['zipcode'] = reqzipcode
                #######################################
                print("Login success!")
                return landingPage()
            else:
                print("Login failed, wrong password...")
                return loginPage(wrongPassword=True, notExist=False)
        else:
            print("User does not exist...")
            return loginPage(wrongPassword=False, notExist=True)

    except Exception as e:
        print(str(e))
        return "System Error"

def getUserRole(reqUsername):
    try:
        cur.execute("SELECT * FROM train_reservation.customer, train_reservation.user_ "
                    + "WHERE train_reservation.user_.username = train_reservation.customer.username "
                    + "AND train_reservation.user_.username = %s", (reqUsername,))
        isCustomer = cur.fetchall()

        cur.execute("SELECT * FROM train_reservation.system_admin, train_reservation.user_ "
                    + "WHERE train_reservation.user_.username = train_reservation.system_admin.username "
                    + "AND train_reservation.user_.username = %s", (reqUsername,))
        isSystemAdmin = cur.fetchall()

        cur.execute("SELECT * FROM train_reservation.train_conductor, train_reservation.user_ "
                    + "WHERE train_reservation.user_.username = train_reservation.train_conductor.username "
                    + "AND train_reservation.user_.username = %s", (reqUsername,))
        isTrainConductor = cur.fetchall()

        if isCustomer:
            session['customer_id'] = isCustomer[0][0]
            # print("customer")
            return "customer"
        elif isSystemAdmin:
            session['admin_id'] = isSystemAdmin[0][0]
            # print("system_admin")
            return "system_admin"
        elif isTrainConductor:
            session['conductor_id'] = isTrainConductor[0][0]
            # print("train_conductor")
            return "train_conductor"
        else:
            print("Error: Unregistered to any role")
            return "none"

    except Exception as e:
        print(str(e))
        return "System Error"

@app.route("/logout")
def logout():
    session.clear()
    session['logged_in'] = False
    return home()

########################################## Made by Kiki #############################################
@app.route('/order_food') 
def order_food_page():
    if session.get('logged_in'): 
        try:
            name = (session.get('name'))
            cur.execute("SELECT customer_id FROM train_reservation.user_, train_reservation.customer "
                        + "WHERE train_reservation.user_.username = train_reservation.customer.username "
                        + "AND train_reservation.user_.username = %s", [name])
            customerId = cur.fetchone()

            cur.execute("SELECT date, food_name, quantity, price FROM train_reservation.food, train_reservation.food_order, train_reservation.customer "
                        + "WHERE train_reservation.food.food_id = train_reservation.food_order.food_id "
                        + "AND train_reservation.customer.customer_id = train_reservation.food_order.customer_id "
                        + "AND train_reservation.customer.customer_id = %s", customerId)
            orderList=cur.fetchall()

            for x in range (0, len(orderList)):
                qty = orderList[x][2]
                price = orderList[x][3]
                totalPrice = int(qty) * float(price)
                orderList[x]+=(totalPrice,)

            # print(orderList)
            cur.execute("SELECT food_name, price from train_reservation.food "
                        + "ORDER BY price ASC ")
            foodList = cur.fetchall()
            # print(foodList)
            for x in range (0, len(foodList)):
                foodList[x] += (x,)
                # print(foodList[x][0])
            # print(foodList)
            foodListSize = len(foodList)
            return render_template('order_food.html', food_list = foodList, size = foodListSize, order_list = orderList)
        
        except Exception as e:
            print(str(e))
            return "System Error"
    else:
        return render_template('login.html', sessionClosed = True)

@app.route('/add_order_food', methods=['POST']) 
def add_order_food():
    if session.get('logged_in'): 
        try:
            if request.method == 'POST':
                reqList = request.json
        
                nameList=[]
                priceList=[]
                qtyList=[]
                # totalPriceList=[]

                name = (session.get('name'))
                cur.execute("SELECT customer_id FROM train_reservation.user_, train_reservation.customer "
                            + "WHERE train_reservation.user_.username = train_reservation.customer.username "
                            + "AND train_reservation.user_.username = %s", [name])
                customerId = cur.fetchone()

                for data in reqList:
                    food = reqList[data]
                    nameList.append(food["name"])
                    priceList.append(food["price"])
                    qtyList.append(food["qty"])

                for x in range (0, len(nameList)):
                    # print("-",nameList[x],":", qtyList[x])
            
                    cur.execute("SELECT food_id FROM train_reservation.food "
                                + "WHERE train_reservation.food.food_name = %s", [nameList[x]])
                    foodId = cur.fetchone()
                    # print(foodId)
                    cur.execute("SELECT setval('train_reservation.train_foodorder_id_seq', (SELECT max(train_reservation.food_order.food_order_id) FROM train_reservation.food_order))")
                    cur.execute("INSERT INTO train_reservation.food_order (date, customer_id, food_id, quantity) "
                                + " VALUES (%s, %s, %s, %s)", (now.strftime("%Y-%m-%d"), customerId, foodId, qtyList[x]))
                    print("success")

                return jsonify(data = "success")

        except Exception as e:
            print(str(e))
            return "System Error"
    else:
        return render_template('login.html', sessionClosed = True)

########################################## Made by Kiki #############################################
@app.route('/attendance')
def attendancePage():
    if session.get('logged_in'):
        try:
            name = (session.get('name'))

        
            cur.execute("SELECT date, check_in_time, check_out_time from train_reservation.user_, train_reservation.train_conductor, train_reservation.conductor_attendance "
                        + "WHERE train_reservation.user_.username = train_reservation.train_conductor.username "
                        + "AND train_reservation.train_conductor.conductor_id = train_reservation.conductor_attendance.id_conductor "
                        + "AND train_reservation.user_.username = %s", [name])
            attendanceList = cur.fetchall()
        
            # print(attendanceList)
            return render_template('attendance.html', attendance_list = attendanceList)
        
        except Exception as e:
            print(str(e))
            return "System Error"
    else:
        return render_template('login.html', sessionClosed = True)
    

@app.route('/add_attendance', methods=[ 'POST'])
def add_attendance():
    if session.get('logged_in'):
        try:
            if request.method == 'POST':

                reqList = request.json
                # print(reqList)
                name = (session.get('name'))
                # print("0")
                cur.execute("SELECT conductor_id from train_reservation.user_, train_reservation.train_conductor "
                            + "WHERE train_reservation.user_.username = train_reservation.train_conductor.username "
                            + "AND train_reservation.user_.username = %s", [name])
                conductorId = cur.fetchone()        

                # print(conductorId[0])        

                date = reqList["date"]
                checkIn = reqList["in"]
                checkOut = reqList["out"]
            
                cur.execute("SELECT setval('train_reservation.train_attendance_id_seq', (SELECT max(train_reservation.conductor_attendance.attendance_id) FROM train_reservation.conductor_attendance))")
                cur.execute("INSERT INTO train_reservation.conductor_attendance (date, check_in_time, check_out_time, id_conductor) "
                            + " VALUES (%s, %s, %s, %s)", (date, checkIn, checkOut, conductorId[0]))
            
                print("add attendance success")

                return jsonify(data = "success")
                

        except Exception as e:
            print(str(e))
            return "System Error"
    else:
        return render_template('login.html', sessionClosed = True)

########################################## Made by Vincent ##########################################
@app.route('/landing_page')
def landingPage():
    if session.get('logged_in'):
        return render_template('landing_page.html')
    else:
        return render_template('login.html', sessionClosed = True)

########################################## Made by Bayu ##########################################
@app.route('/profile') #landing page profile button redirects to profile.html not profile
def profile(): # role will be none because data was randomly generated for each table, users will almost certainly will not be in a "role" table
    if session.get('logged_in'):
        return render_template('profile.html', username = session['name'], role = session['role'], email = session['email'],
                                phone = session['phone_no'], address_no = session['address_no'],
                                street = session['street'], village = session['village'], sub_district = session['sub_district'], zip_code = session['zipcode'])
    else:
        return render_template('login.html', sessionClosed = True)

########################################## Made by Bayu ##########################################
@app.route('/station')
def stationUpdatePage():
    if session.get('logged_in'):
        cur.execute("SELECT * FROM train_reservation.station")
        stationList = cur.fetchall()
        return render_template('station_update.html', station_list = stationList)
    else:
        return render_template('login.html', sessionClosed = True)

@app.route('/setoldstation', methods=['POST'])
def setoldstation():
    if session.get('logged_in'):
        # print("in setoldStation method")
        try:
            stationName = request.json
            cur.execute("SELECT * FROM train_reservation.station WHERE station_name = %s", (stationName,))
            old_station = cur.fetchall()[0]
            # print(old_station)
            global oldStationName
            global oldLatitude
            global oldLongitude     
            global oldStreet
            global oldCity
            global oldZipcode
            global oldTrack
            oldStationName = old_station[1]
            oldLatitude = old_station[2]
            oldLongitude = old_station[3]     
            oldStreet = old_station[4]
            oldCity = old_station[5]
            oldZipcode = old_station[6]
            oldTrack = old_station[7]
            # fills out old section of form
            data1 = {"station_name" : oldStationName, "latitude" : oldLatitude, "longitude" :  oldLatitude, "city" : oldCity,
                    "street" : oldStreet, "zipcode" : oldZipcode, "track" : oldTrack}
            # print("before return")
            return jsonify(data = data1)
        except Exception as e:
            print(str(e))
            return "System Error"
    else:
        return render_template('login.html', sessionClosed = True)

@app.route('/updatethestation', methods=['POST'])
def updatethestation():
    if session.get('logged_in'):
        try:
            new_station_name = request.form["new_station_name"]    
            new_latitude = request.form["new_latitude"]
            new_longitude = request.form["new_longitude"]
            new_street = request.form["new_street"]
            new_city = request.form["new_city"]
            new_zipcode = request.form["new_zipcode"]
            new_track = request.form["new_track"]

            cur.execute("UPDATE train_reservation.station " +
                        "SET station_name = %s, latitude = %s, " +
                        "longitude = %s, street = %s, " +
                        "city = %s, zipcode = %s, " +
                        "available_track = %s " +
                        "WHERE station_name = %s AND latitude = %s AND " +
                        "longitude = %s AND street = %s AND " +
                        "city = %s AND zipcode = %s AND " +
                        "available_track = %s", (new_station_name, new_latitude, new_longitude, new_street, new_city, new_zipcode, new_track,
                                    oldStationName, oldLatitude, oldLongitude, oldStreet, oldCity, oldZipcode, oldTrack))
            return stationUpdatePage()
        
        except Exception as e:
            print(str(e))
            return "System Error"
    else:
        return render_template('login.html', sessionClosed = True)

########################################## Made by Vincent ##########################################
@app.route('/book_train')
def bookTrainPage():
    if session.get('logged_in'):
        try:
            dict_cur.execute("SELECT * from train_reservation.station")
            stationData = dict_cur.fetchall()
            # print(len(stationData))
            # session['station_data'] = stationData
            return render_template('book_train.html', station_data = stationData)

        except Exception as e:
            print(str(e))
            return "System Error"
    else:
        return render_template('login.html', sessionClosed = True)

@app.route('/get_schedule', methods=['POST', 'GET'])
def showSchedule():
    if session.get('logged_in'):
        date = request.args.get('date')
        origin_station_id = request.args.get('origin_station_id')
        destination_station_id = request.args.get('destination_station_id')
        # print(date)
        # print(origin_station_id)
        # print(destination_station_id)
        try:
            dict_cur.execute("SELECT train_reservation.train.train_name, train_reservation.train.class, train_reservation.train.train_id, train_reservation.train_schedule.schedule_id "
                            +"FROM train_reservation.route, train_reservation.train, train_reservation.train_schedule "
                            +"WHERE train_reservation.route.origin_station = %s AND train_reservation.route.destination_station = %s "
                            +"AND train_reservation.route.route_id = train_reservation.train.route_id "
                            +"AND train_reservation.train.train_id = train_reservation.train_schedule.train_id "
                            +"AND train_reservation.train_schedule.date = %s", (origin_station_id, destination_station_id, date))
            scheduleData = dict_cur.fetchall()

            for data in scheduleData:
                if data['class'] == 'executive':
                    data['price'] = 2000000
                elif data['class'] == 'business':
                    data['price'] = 1000000
                else:
                    data['price'] = 500000

            # print(scheduleData)
            return jsonify(scheduleData = scheduleData)

        except Exception as e:
            print(str(e))
            return jsonify(scheduleData = "ERROR")
    else:
        return render_template('login.html', sessionClosed = True)

@app.route('/check_available_seat', methods=['POST', 'GET'])
def checkSeat():
    if session.get('logged_in'):
        train_id = request.args.get('train_id')
        # print(train_id)
        try:
            # testing with train_id 1 because train_id 3 don't have seat at the moment
            dict_cur.execute("SELECT distinct train_reservation.carriage.carriage_id "
                            +"FROM train_reservation.carriage, train_reservation.train "
                            +"WHERE train_reservation.carriage.train_id = %s "
                            +"ORDER BY train_reservation.carriage.carriage_id asc", (train_id, ))
            carriageData = dict_cur.fetchall()
            # print(carriageData)
            
            finalData = {}
            for data in carriageData:
                carriage_id = data['carriage_id']
                # print(carriage_id)
                dict_cur.execute("SELECT distinct train_reservation.seat.seat_id "
                                +"FROM train_reservation.carriage, train_reservation.seat "
                                +"WHERE train_reservation.seat.carriage_id = %s "
                                +"AND train_reservation.seat.seat_id "
                                +"NOT IN (SELECT train_reservation.passenger.seat_id "
                                        +"FROM train_reservation.passenger) "
                                +"ORDER BY train_reservation.seat.seat_id asc", (carriage_id, ))
                seatData = dict_cur.fetchall()
                finalData[carriage_id] = seatData

            # print(finalData)
            return jsonify(finalData = finalData)

        except Exception as e:
            print(str(e))
            return jsonify(carriageData = "ERROR")
    else:
        return render_template('login.html', sessionClosed = True)

@app.route('/book_passenger', methods=['POST'])
def bookPassenger():
    if session.get('logged_in'):
        idCardUsed = False
        customer_id = session.get('customer_id')
        if request.method == 'POST':
            passengersData = request.json

            cur.execute("SELECT train_reservation.passenger.id_card FROM train_reservation.passenger")
            id_cards_tuple = cur.fetchall()
            id_cards_list = []
            for tuple_ in id_cards_tuple:
                id_cards_list.append(tuple_[0])
            # print(id_cards_list)
            
            for data in passengersData:
                passenger = passengersData[data]
                id_card = passenger['id_card']
                if id_card in id_cards_list:
                    idCardUsed = True

            if (not idCardUsed) and (customer_id is not None):
                try:
                    for data in passengersData:
                        passenger = passengersData[data]
                        id_card = passenger['id_card']
                        name = passenger['name']
                        phone_no = passenger['phone_no']
                        dob = passenger['dob']
                        seat_id = passenger['seat_id']
                        schedule_id = passenger['schedule_id']

                        dict_cur.execute("INSERT INTO train_reservation.passenger (id_card, name, phone_no, dob, seat_id) "
                                        +"VALUES (%s, %s, %s, %s, %s)", (id_card, name, phone_no, dob, seat_id))
                        dict_cur.execute("SELECT setval('train_reservation.train_booking_id_seq', (SELECT max(train_reservation.train_booking.booking_id) FROM train_reservation.train_booking))")
                        dict_cur.execute("INSERT INTO train_reservation.train_booking (booking_id, booking_date, schedule_id, passenger_id, booked_by) "
                                        +"VALUES (default, %s, %s, %s, %s)", 
                                        (datetime.datetime.now().date(), schedule_id, id_card, customer_id))
                    return jsonify(data = "BOOK SUCCESS")

                except Exception as e:
                    print(str(e))
                    return jsonify(data = "DATA ERROR")
            else:
                return jsonify(data="BOOK FAILED")

        else:
            return jsonify(data="METHOD ERROR")
    else:
        return render_template('login.html', sessionClosed = True)

########################################## Made by Vincent ##########################################
@app.route('/e_ticket')
def eTicketPage():
    if session.get('logged_in'):
        try:
            customer_id = session.get('customer_id')
            bookingDict = {}
            # print(customer_id)
            if (customer_id is not None):
                cur.execute("SELECT * FROM train_reservation.train_booking "
                        +"WHERE train_reservation.train_booking.booked_by = %s", (customer_id,))
                bookingData = cur.fetchall() 
                idx = 1
                for ticket in bookingData:
                    bookingDict[idx] = ticket
                    idx += 1
                # print(bookingDict)

                return render_template('e_ticket.html', bookingData = bookingDict)
            else:
                return "System Error"
        except Exception as e:
            print(str(e))
            return "System Error"
    else:
        return render_template('login.html', sessionClosed = True)

@app.route('/see_ticket_detail', methods=['POST'])
def seeTicketDetail():
    if session.get('logged_in'):
        if request.method == 'POST':
            try:
                bookingId = request.json
                
                dict_cur.execute("SELECT train_reservation.train_schedule.date, train_reservation.train.train_name, train_reservation.route.origin_station, train_reservation.route.destination_station, train_reservation.passenger.name, train_reservation.seat.seat_id, train_reservation.carriage.carriage_id "
                                +"FROM train_reservation.train_booking, train_reservation.train_schedule, train_reservation.train, train_reservation.route, train_reservation.passenger, train_reservation.seat, train_reservation.carriage "
                                +"WHERE train_reservation.train_booking.booking_id = %s AND train_reservation.train_booking.schedule_id = train_reservation.train_schedule.schedule_id AND train_reservation.train_booking.passenger_id = train_reservation.passenger.id_card "
                                +"AND train_reservation.passenger.seat_id = train_reservation.seat.seat_id AND train_reservation.seat.carriage_id = train_reservation.carriage.carriage_id "
                                +"AND train_reservation.carriage.train_id = train_reservation.train.train_id AND train_reservation.train.route_id = train_reservation.route.route_id", (bookingId, ))
                ticketDetail = dict_cur.fetchall()
                origin_station_id = ticketDetail[0]['origin_station']
                destination_station_id = ticketDetail[0]['destination_station']

                dict_cur.execute("SELECT train_reservation.station.station_name "
                                +"FROM train_reservation.station "
                                +"WHERE train_reservation.station.station_id = %s", (origin_station_id, ))
                origin_station_name = dict_cur.fetchall()[0]['station_name']

                dict_cur.execute("SELECT train_reservation.station.station_name "
                                +"FROM train_reservation.station "
                                +"WHERE train_reservation.station.station_id = %s", (destination_station_id, )) 
                destination_station_name = dict_cur.fetchall()[0]['station_name']

                ticketDetail[0]['origin_station'] = origin_station_name
                ticketDetail[0]['destination_station'] = destination_station_name           

                # print(ticketDetail)
                return jsonify(data = ticketDetail)
            except Exception as e:
                print(str(e))
                return "System Error"
        else:
            return jsonify(data="METHOD ERROR")
    else:
        return render_template('login.html', sessionClosed = True)

# main method to run the web server
if __name__ == '__main__':
    app.run(debug = True)
